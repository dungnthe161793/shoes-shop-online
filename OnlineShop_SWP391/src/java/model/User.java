/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

/**
 *
 * @author ADMIN
 */
import java.sql.Date;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor

public class User {

    private int userId;
    private String fullName;
    private String email;
    private String password;
    private String avatar;
    private boolean gender;
    private String mobile;
    private String address;
    private int roleId;
    private int status;
    private FeedBack feedback;
    private Product product;
    private String feedbackId;

    public User(int id) {
        this.userId = id;
    }

    public User(int userId, int roleId, int status) {
        this.userId = userId;
        this.roleId = roleId;
        this.status = status;
    }

    public User(int userId, int roleId, int status, String fullName) {
        this.userId = userId;
        this.roleId = roleId;
        this.status = status;
        this.fullName = fullName;
    }

    public User(String email, String password) {
        this.email = email;
        this.password = password;
    }

    public User(int id, String fullname) {
        this.userId = id;
        this.fullName = fullname;
    }

    public User(int id, String email, String password) {
        this.userId = id;
        this.email = email;
        this.password = password;
    }

    public User(int id, String fullname, String avatar, int status) {
        this.userId = id;
        this.fullName = fullname;
        this.avatar = avatar;
        this.status = status;
    }

    public User(int id, String fullname, String email, String avatar, boolean gender, String mobile, String address, int role, int status) {
        this.userId = id;
        this.fullName = fullname;
        this.email = email;
        this.avatar = avatar;
        this.gender = gender;
        this.mobile = mobile;
        this.address = address;
        this.roleId = role;
        this.status = status;
    }

    public User(int id, String fullname, boolean gender, String email, String mobile, int role, int status) {
        this.userId = id;
        this.fullName = fullname;
        this.email = email;
        this.gender = gender;
        this.mobile = mobile;
        this.roleId = role;
        this.status = status;
    }

    public User(int id, String fullname, String email, String password, String avatar, boolean gender, String mobile, String address, int status) {
        this.userId = id;
        this.fullName = fullname;
        this.email = email;
        this.password = password;
        this.avatar = avatar;
        this.gender = gender;
        this.mobile = mobile;
        this.address = address;
        this.status = status;
    }

    public User(int id, String fullname, String email, String mobile, Product product, FeedBack feedback) {
        this.userId = id;
        this.fullName = fullname;
        this.email = email;
        this.mobile = mobile;

        this.product = product;
        this.feedback = feedback;

    }

    public User(int userId, String fullName, String email, String password, String avatar, boolean gender, int status) {
        this.userId = userId;
        this.fullName = fullName;
        this.email = email;
        this.password = password;
        this.avatar = avatar;
        this.gender = gender;
        this.mobile = mobile;
        this.status = status;
    }

    public User(int id, String fullname, String email, String mobile, String id_feedback, Product product, FeedBack feedback) {
        this.userId = id;
        this.fullName = fullname;
        this.email = email;
        this.mobile = mobile;
        this.feedbackId = id_feedback;
        this.product = product;
        this.feedback = feedback;

    }

}
