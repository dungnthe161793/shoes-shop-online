/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import java.util.ArrayList;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 *
 * @author ADMIN
 */
@Getter
@Setter

public class Cart {

    private ArrayList<Item> items;

    public Cart() {
        items = new ArrayList<>();
    }

    public Cart(ArrayList<Item> items) {
        this.items = items;
    }

    public int getQuantityById(int id, int size) {
        return getItemByIdAndSize(id, size).getQuantity();
    }



    private Item getItemByIdAndSize(int id, int size) {
        for (Item item : items) {
            if (item.getSize().getSizeName() == size && item.getProduct().getProductid() == id) {
                return item;
            }
        }
        return null;
    }

    public void addItem(Item item) {
        if (getItemByIdAndSize(item.getProduct().getProductid(), item.getSize().getSizeName()) != null) {
            Item newItem = getItemByIdAndSize(item.getProduct().getProductid(), item.getSize().getSizeName());
            newItem.setQuantity(item.getQuantity()+ newItem.getQuantity());
        } else {
            items.add(item);
        }
    }

    public void removeItem(int id, int size) {
        if (getItemByIdAndSize(id, size) != null) {
            items.remove(getItemByIdAndSize(id, size));
        }
    }

    public double getTotalMoney() {
        double total = 0;
        for (Item item : items) {
            total += (item.getQuantity() * item.getProduct().getSalePrices());
        }
        return total;
    }

}
