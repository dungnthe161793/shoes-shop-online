/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

/**
 *
 * @author ADMIN
 */
import java.sql.Date;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor

public class ProductImages {

    private int productImagesId;
    private String images;
    public Product product;

    public ProductImages(int productImagesId, String images) {
        this.productImagesId = productImagesId;
        this.images = images;
    }

    public ProductImages(String images, Product product) {
        this.images = images;
        this.product = product;
    }

    
    @Override
    public String toString() {
        return "ProductImages{" + "productImagesId=" + productImagesId + ", images=" + images + '}';
    }

}
