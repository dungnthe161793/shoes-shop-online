/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

import java.sql.Date;

/**
 *
 * @author ADMIN
 */
import java.sql.Date;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor

public class Order {

    private int orderid;
    private Date orderdate;
    private Double totalCost;
    private String fullname;
    private String mobile;
    private String address;
    private int salerId;
    private String note;
    public StatusOrder statusOrder;
    public OrderDetail orderDetail;
    public Size size;

    public Order(int orderid, Date orderdate, Double totalCost, StatusOrder statusOrder) {
        this.orderid = orderid;
        this.orderdate = orderdate;
        this.totalCost = totalCost;
        this.statusOrder = statusOrder;
    }

    public Order(int orderid, Date orderdate, OrderDetail orderDetail) {
        this.orderid = orderid;
        this.orderdate = orderdate;
        this.orderDetail = orderDetail;
    }

    public Order(int orderid, Date orderdate, OrderDetail orderDetail, Size size) {
        this.orderid = orderid;
        this.orderdate = orderdate;
        this.orderDetail = orderDetail;
        this.size = size;
    }

    public Order(String fullname, String mobile, String address, String note) {
        this.fullname = fullname;
        this.mobile = mobile;
        this.address = address;
        this.note = note;
    }

    public Order(int orderid, Date orderdate, String fullname, StatusOrder statusOrder) {
        this.orderid = orderid;
        this.orderdate = orderdate;
        this.fullname = fullname;
        this.statusOrder = statusOrder;
    }

    public Order(int orderid, Date orderdate, Double totalCost, String fullname, StatusOrder statusOrder) {
        this.orderid = orderid;
        this.orderdate = orderdate;
        this.totalCost = totalCost;
        this.fullname = fullname;
        this.statusOrder = statusOrder;
    }

    public Order(int orderid, Date orderdate, Double totalCost, String fullname, String mobile, String address, int salerId, String note, StatusOrder statusOrder, OrderDetail orderDetail) {
        this.orderid = orderid;
        this.orderdate = orderdate;
        this.totalCost = totalCost;
        this.fullname = fullname;
        this.mobile = mobile;
        this.address = address;
        this.salerId = salerId;
        this.note = note;
        this.statusOrder = statusOrder;
        this.orderDetail = orderDetail;
    }

}
