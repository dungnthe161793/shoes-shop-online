/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package model;

/**
 *
 * @author ADMIN
 */
import java.sql.Date;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 *
 * @author ADMIN
 */
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor

public class Product {

    private int productid;
    private String productname;
    private double originalPrices;
    private double salePrices;
    private String ProductDetails;
    private String briefInfor;
    private boolean status;
    private Date updateDate;
    private int quantity;
    private int totalQuantity;
    public ProductImages productImg;
    public Category category;
    private OrderDetail orderdtail;
    private User user;
    
    public Product(int productid) {
        this.productid = productid;
    }

    public Product(int productid, String productname, double originalPrices, double salePrices, String ProductDetails, String briefInfor, boolean status, Date updateDate, Category category) {
        this.productid = productid;
        this.productname = productname;
        this.originalPrices = originalPrices;
        this.salePrices = salePrices;
        this.ProductDetails = ProductDetails;
        this.briefInfor = briefInfor;
        this.status = status;
        this.updateDate = updateDate;
        this.category = category;
    }

    public Product(int productid, String productname, double originalPrices, double salePrices, String ProductDetails, String briefInfor, boolean status, Date updateDate, ProductImages productImg) {
        this.productid = productid;
        this.productname = productname;
        this.originalPrices = originalPrices;
        this.salePrices = salePrices;
        this.ProductDetails = ProductDetails;
        this.briefInfor = briefInfor;
        this.status = status;
        this.updateDate = updateDate;
        this.productImg = productImg;

    }

    public Product(int productid, String productname, double originalPrices, double salePrices, String ProductDetails, String briefInfor, boolean status, Date updateDate, ProductImages productImg, Category category) {
        this.productid = productid;
        this.productname = productname;
        this.originalPrices = originalPrices;
        this.salePrices = salePrices;
        this.ProductDetails = ProductDetails;
        this.briefInfor = briefInfor;
        this.status = status;
        this.updateDate = updateDate;
        this.productImg = productImg;
        this.category = category;
    }

    public Product(int productid, String productname, int totalQuantity) {
        this.productid = productid;
        this.productname = productname;
        this.totalQuantity = totalQuantity;
    }

    public Product(String productname, int productid, double salePrices) {
        this.productname = productname;
        this.productid = productid;
        this.salePrices = salePrices;
    }

    public Product(String productname, double salePrices) {
        this.productname = productname;

        this.salePrices = salePrices;
    }

    public Product(String productname) {
        this.productname = productname;
    }

    
    public Product(int productid, String productname, OrderDetail orderdtail) {
        this.productid = productid;
        this.productname = productname;
        this.orderdtail = orderdtail;
    }

    public Product(int productid, String productname, double originalPrices, double salePrices, String ProductDetails, String briefInfor, boolean status, Date updateDate, ProductImages productImg, Category category, int quantity) {
        this.productid = productid;
        this.productname = productname;
        this.originalPrices = originalPrices;
        this.salePrices = salePrices;
        this.ProductDetails = ProductDetails;
        this.briefInfor = briefInfor;
        this.status = status;
        this.updateDate = updateDate;
        this.quantity = quantity;
        this.productImg = productImg;
        this.category = category;
    }

    public Product(int productid, String productname, double originalPrices, double salePrices, Category category, int totalQuantity) {
        this.productid = productid;
        this.productname = productname;
        this.originalPrices = originalPrices;
        this.salePrices = salePrices;
        this.totalQuantity = totalQuantity;
        this.category = category;
    }

    public Product(String productname, double salePrices, User user) {
        this.productname = productname;
        this.salePrices = salePrices;
        this.user = user;
    }


    @Override
    public String toString() {
        return "Product{" + "productid=" + productid + ", productname=" + productname + ", originalPrices=" + originalPrices + ", salePrices=" + salePrices + ", ProductDetails=" + ProductDetails + ", briefInfor=" + briefInfor + ", status=" + status + ", updateDate=" + updateDate + ", productImg=" + productImg.toString();
    }

}
