/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.Admin;

import dao.ProductDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.util.ArrayList;
import model.Size;

/**
 *
 * @author Admin
 */
@WebServlet(name = "EditAndUpdateSizeProductManager", urlPatterns = {"/editandupdatesizeproductmanager"})
public class EditAndUpdateSizeProductManager extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet EditSizeProductManager</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet EditSizeProductManager at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        ProductDAO dao = new ProductDAO();
        String pid = request.getParameter("pid");
        ArrayList<Size> listSz = dao.getSizeProductById(pid);
        //Update size product
        String upSizeName = "";
        String upSizeQuantity = "";
        for (Size size : listSz) {
            upSizeName = request.getParameter("size" + size.getSizeName());
            upSizeQuantity = request.getParameter("quantity" + upSizeName);
            //Update quantity of size product
            dao.updateSizeOfProduct(upSizeQuantity, upSizeName, pid);
            //Update status of size product
            dao.updateStatusOfSizeProduct(Integer.parseInt(upSizeQuantity), Integer.parseInt(upSizeName), Integer.parseInt(pid));
        }
        // Insert size product
        HttpSession session = request.getSession();
        String inSize = request.getParameter("size");
        String inQuantity = request.getParameter("quantity");
        String notificate = "";
        if(inSize != null ){
            if(inQuantity == null || inQuantity.isEmpty()){
                inQuantity = "1";
            }
        }
        if (inSize != null && !inSize.isEmpty()) {
            //Check size product have duplicate?
            if (!checkDuplicateSizeProduct(Integer.parseInt(inSize), listSz)) {
                dao.insertSizeOfProduct(inSize, inQuantity, pid);
                notificate = "Cập nhật thành công";
            } else {
                notificate = "Sản phẩm đã có size này";
            }
        }
        session.setAttribute("notificate", notificate);
        response.sendRedirect("editproductmanager?id=" + pid);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    public static boolean checkDuplicateSizeProduct(int sizeName, ArrayList<Size> listSize) {
        for (Size size : listSize) {
            if (sizeName == size.getSizeName()) {
                return true;
            }
        }
        return false;
    }
}
