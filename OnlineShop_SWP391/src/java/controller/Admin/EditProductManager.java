/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.Admin;

import LocationFile.Location;
import dao.ProductDAO;
import jakarta.servlet.ServletContext;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.MultipartConfig;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.Part;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import model.Category;
import model.Product;
import model.ProductImages;
import model.Size;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

/**
 *
 * @author Admin
 */
@MultipartConfig(fileSizeThreshold = 1024 * 1024 * 2,
        maxFileSize = 1024 * 1024 * 10,
        maxRequestSize = 1024 * 1024 * 50)
@WebServlet(name = "EditProductManager", urlPatterns = {"/editproductmanager"})
public class EditProductManager extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet EditProductManager</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet EditProductManager at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String pid = request.getParameter("id");
        ProductDAO dao = new ProductDAO();

        //get product detail by id
        Product product = dao.getProductDetail(pid);
        //get sub-photos of the product 
        ArrayList<ProductImages> listSubImg = dao.getAllImageProductbyID(pid);
        ArrayList<Category> listCategory = dao.getallCategory();
        //get size of Product
        ArrayList<Size> listSz = dao.getSizeProductById(pid);

        request.setAttribute("size", listSz);
        request.setAttribute("listSubImg", listSubImg);
        request.setAttribute("p", product);
        request.setAttribute("listCa", listCategory);

        request.getRequestDispatcher("./Manager/EditProductManager.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Location lo = new Location();
        ProductDAO dao = new ProductDAO();
        Date currentDate = new Date();

        String pid = request.getParameter("pid");
        String productName = request.getParameter("productName");
        String category = request.getParameter("category");
        String statusProduct = request.getParameter("statusProduct");
        String salePrices = request.getParameter("salePrices");
        String originalPrices = request.getParameter("originalPrices");
        Part file = request.getPart("ImageUpload");
        String ImageUpload = file.getSubmittedFileName();
        String brefInfo = request.getParameter("brefInfo");
        String productDetail = request.getParameter("productDetail");
        String formattedDate = formatDate(currentDate, "yyyy-MM-dd HH:mm:ss");

        if (ImageUpload != null && ImageUpload.isEmpty()) {
            Product p = dao.getProductDetail(pid);
            ImageUpload = p.getProductImg().getImages();
        }

        //load image to file img
        DiskFileItemFactory factory = new DiskFileItemFactory();
        ServletContext servletContext = this.getServletConfig().getServletContext();
        File repository = (File) servletContext.getAttribute("javax.servlet.context.tempdir");
        factory.setRepository(repository);
        ServletFileUpload upload = new ServletFileUpload(factory);
        upload.setHeaderEncoding("UTF-8");
        OutputStream out = null;
        InputStream filecontent = null;
        StringBuilder absolutePath = Location.getRelativePath(request.getServletContext(), lo.LOADIMAGETOFILEIMG);
        try {
            File file1 = new File(absolutePath+ ImageUpload);
            out = new FileOutputStream(file1);
            filecontent = file.getInputStream();
            int read;
            final byte[] bytes = new byte[1024];
            while ((read = filecontent.read(bytes)) != -1) {
                out.write(bytes, 0, read);
            }
        } catch (FileNotFoundException fne) {
            System.out.println(fne);
        }
        
        dao.updateImgProductDetail(pid, ImageUpload);
        dao.updateProductDetail(productName, originalPrices, salePrices, productDetail, brefInfo, statusProduct, category, formattedDate, pid);
        response.sendRedirect("editproductmanager?id=" + pid);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    public static String formatDate(Date date, String format) {
        SimpleDateFormat dateFormat = new SimpleDateFormat(format);
        return dateFormat.format(date);
    }

}
