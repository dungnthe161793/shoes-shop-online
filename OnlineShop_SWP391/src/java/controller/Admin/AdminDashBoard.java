/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.Admin;

import dao.AdminDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import model.FeedBack;
import model.OrderDetail;
import model.Product;
import model.User;

/**
 *
 * @author Admin
 */
@WebServlet(name = "AdminDashBoardControl", urlPatterns = {"/admindashboard"})
public class AdminDashBoard extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        AdminDAO dao = new AdminDAO();
        //get data to chart on admin dashbroad
        int totalordertrans = dao.getTotalOrderTransport();
        int totalordersuccess = dao.getTotalOrderSuccess();
        int totalordercancel = dao.getTotalOrderCancel();
        ArrayList<Integer> dataValues = new ArrayList<>();
        dataValues.add(totalordertrans);
        dataValues.add(totalordersuccess);
        dataValues.add(totalordercancel);
        request.setAttribute("dataValues", dataValues);
        
        int totalcustomer = dao.getTotalCustomer();
        int totalrevenue = dao.getTotalRevenue();
        double avgratedstar = dao.getAvarageStarFeeback();

        ArrayList<OrderDetail> listod = dao.getTotalRevenueByCategory();
        ArrayList<FeedBack> listfb = dao.getTotalFeedBackByCategory();
        ArrayList<Product> listp = dao.getTopSell();

        request.setAttribute("totalordertrans", totalordertrans);
        request.setAttribute("totalordersuccess", totalordersuccess);
        request.setAttribute("totalordercancel", totalordercancel);
        request.setAttribute("totalcustomer", totalcustomer);
        request.setAttribute("avgrate", avgratedstar);
        request.setAttribute("totalrevenue", totalrevenue);
        request.setAttribute("totalrevenuebycategory", listod);
        request.setAttribute("listp", listp);
        User uRe = dao.getNewLyRegisterUser();
        User uBo = dao.getNewLyBoughtUser();
        request.setAttribute("totalfbbycategory", listfb);
        request.setAttribute("uRe", uRe);
        request.setAttribute("uBo", uBo);
        request.getRequestDispatcher("./Manager/AdminDashBoard.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //Load data again on admin dashbroad
        AdminDAO dao = new AdminDAO();
        int totalordertrans = dao.getTotalOrderTransport();
        int totalordersuccess = dao.getTotalOrderSuccess();
        int totalordercancel = dao.getTotalOrderCancel();
        ArrayList<Integer> dataValues = new ArrayList<>();
        dataValues.add(totalordertrans);
        dataValues.add(totalordersuccess);
        dataValues.add(totalordercancel);
        request.setAttribute("dataValues", dataValues);
        
        int totalcustomer = dao.getTotalCustomer();
        int totalrevenue = dao.getTotalRevenue();
        double avgratedstar = dao.getAvarageStarFeeback();

        ArrayList<OrderDetail> listod = dao.getTotalRevenueByCategory();
        ArrayList<FeedBack> listfb = dao.getTotalFeedBackByCategory();
        ArrayList<Product> listp = dao.getTopSell();

        request.setAttribute("totalordertrans", totalordertrans);
        request.setAttribute("totalordersuccess", totalordersuccess);
        request.setAttribute("totalordercancel", totalordercancel);
        request.setAttribute("totalcustomer", totalcustomer);
        request.setAttribute("avgrate", avgratedstar);
        request.setAttribute("totalrevenue", totalrevenue);
        request.setAttribute("totalrevenuebycategory", listod);
        request.setAttribute("listp", listp);
        User uRe = dao.getNewLyRegisterUser();
        User uBo = dao.getNewLyBoughtUser();
        request.setAttribute("totalfbbycategory", listfb);
        request.setAttribute("uRe", uRe);
        request.setAttribute("uBo", uBo);
        
        String d1 = request.getParameter("d1");
        String d2 = request.getParameter("d2");
        int totalordersuccess7day = dao.getTotalOrderLast7daySuccess(d1, d2);
        int totalorderall7day = dao.getTotalOrderLast7dayAll(d1, d2);
        request.setAttribute("d1", d1);
        request.setAttribute("d2", d2);

        request.setAttribute("totalsuc7", totalordersuccess7day);
        request.setAttribute("totalall7", totalorderall7day);
        request.getRequestDispatcher("./Manager/AdminDashBoard.jsp").forward(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    public static void main(String[] args) {
        AdminDAO dao = new AdminDAO();
        ArrayList<Product> listp = dao.getTopSell();
        for (Product product : listp) {
            System.out.println(product.getProductname());
        }
    }
}
