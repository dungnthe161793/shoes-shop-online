/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.Admin;

import dao.OrderSaleDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.util.ArrayList;
import model.Order;
import model.StatusOrder;
import model.User;

/**
 *
 * @author Admin
 */
@WebServlet(name = "EditOrder", urlPatterns = {"/editorder"})
public class EditOrder extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet EditOrder</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet EditOrder at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        OrderSaleDAO dao = new OrderSaleDAO();

        String OId = request.getParameter("OId");
        request.setAttribute("Oid", OId);

        //get all Orderdetail byOID
        ArrayList<Order> listO = dao.getOrderDetailByOid(Integer.parseInt(OId));
        request.setAttribute("listO", listO);
        //get all status
        ArrayList<StatusOrder> listStatus = dao.getallStatus();
        request.setAttribute("listStatus", listStatus);
        //get all sale
        ArrayList<User> listSale = dao.getallSale();
        request.setAttribute("listSale", listSale);
        //get user information
        Order ulist = dao.getUserInforOfSale(Integer.parseInt(OId));
        request.setAttribute("listOrder", ulist);

        request.getRequestDispatcher("./Manager/EditOrder.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        OrderSaleDAO dao = new OrderSaleDAO();

        String Oid = request.getParameter("Oid");
        String Sid = request.getParameter("Sid");

        request.setAttribute("Sid", Sid);
        request.setAttribute("Oid", Oid);

        //update status
        dao.UpdateStatus(Integer.parseInt(Sid), Integer.parseInt(Oid));

        //get all orderdetail byoid
        ArrayList<Order> listO = dao.getOrderDetailByOid(Integer.parseInt(Oid));
        request.setAttribute("listO", listO);
        //get all status
        ArrayList<StatusOrder> listStatus = dao.getallStatus();
        request.setAttribute("listStatus", listStatus);
        //get all sale
        ArrayList<User> listSale = dao.getallSale();
        request.setAttribute("listSale", listSale);
        //get user information
        Order ulist = dao.getUserInforOfSale(Integer.parseInt(Oid));
        request.setAttribute("listOrder", ulist);

        request.getRequestDispatcher("./Manager/EditOrder.jsp").forward(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
