/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.Admin;

import dao.OrderSaleDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;
import model.Product;
import model.User;

/**
 *
 * @author Admin
 */
@WebServlet(name = "OrderDashBoard", urlPatterns = {"/orderdashboard"})
public class OrderDashBoard extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet OrderDashBoard</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet OrderDashBoard at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        OrderSaleDAO d = new OrderSaleDAO();

        ArrayList<User> listSale = d.getallSale();
        int a = d.getTotalUser();
        int y = d.getTotalorder();
        request.setAttribute("a", a);
        request.setAttribute("y", y);

        ArrayList<Product> list = d.getTopSelling();
        request.setAttribute("list", list);
        request.setAttribute("listSale", listSale);
        request.getRequestDispatcher("./Manager/OrderDashBoard.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        OrderSaleDAO d = new OrderSaleDAO();
        String option1 = request.getParameter("op");
        String d1 = request.getParameter("d1");
        String d2 = request.getParameter("d2");

        int a = d.getTotalUser();
        int y = d.getTotalorder();
        request.setAttribute("a", a);
        request.setAttribute("y", y);
        request.setAttribute("d1", d1);
        request.setAttribute("d2", d2);

        ArrayList<Product> list = d.getTopSelling();
        request.setAttribute("list", list);
        ArrayList<User> listSale = d.getallSale();

        request.setAttribute("option1", option1);
        request.setAttribute("listSale", listSale);

        int x = d.getTotalorderbyStattus1(option1, d1, d2);
        int b = d.getTotalorderbyStattus2(option1, d1, d2);
        int c = d.getTotalorderbyStattus3(option1, d1, d2);
        int g = d.getTotalorderbyStattus4(option1, d1, d2);
        int e = d.getTotalorderbyStattus5(option1, d1, d2);
        int f = d.getTotalorderbyStattus6(option1, d1, d2);

        List<Integer> dataValues = new ArrayList<>();
        dataValues.add(x);
        dataValues.add(b);
        dataValues.add(c);
        dataValues.add(g);
        dataValues.add(e);
        dataValues.add(f);
        //request.setAttribute("option1", option1);
        request.setAttribute("dataValues", dataValues);
        request.getRequestDispatcher("./Manager/OrderDashBoard.jsp").forward(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
