/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.Client;

import dao.ProductDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.util.ArrayList;
import model.Cart;
import model.Item;
import model.Size;

/**
 *
 * @author Admin
 */
public class EditCartDetail extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet EditCartDetail</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet EditCartDetail at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        Cart cart = null;
        Object ob = session.getAttribute("cart");
        if (ob != null) {
            cart = (Cart) ob;
        } else {
            cart = new Cart();
        }

        String num = request.getParameter("quantity");
        String idItem = request.getParameter("id");
        String size = request.getParameter("size");
        int id, numItem, sizeItem;

        try {
            id = Integer.parseInt(idItem);
            numItem = Integer.parseInt(num);
            sizeItem = Integer.parseInt(size);
            if ((numItem == -1) && (cart.getQuantityById(id, sizeItem) <= 1)) {
                cart.removeItem(id, sizeItem);
            }else {
                ProductDAO dao = new ProductDAO();
                model.Product p = dao.getProductDetail(idItem);
                double price = p.getSalePrices();
                Size sz = dao.getOneSizeProductById(idItem, size);
                Item item = new Item(p, numItem, price, sz);
                cart.addItem(item);
            }
        } catch (NumberFormatException e) {
            System.out.println(e);
        }

        ArrayList<Item> listCart = cart.getItems();
        session.setAttribute("cart", cart);
        session.setAttribute("sizeCart", listCart.size());
        response.sendRedirect("cartdetail");
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        HttpSession session = request.getSession();
        Cart cart = null;
        Object ob = session.getAttribute("cart");
        if (ob != null) {
            cart = (Cart) ob;
        } else {
            cart = new Cart();
        }
        String pid = request.getParameter("pid");
        String size = request.getParameter("size");
        try {
            int id = Integer.parseInt(pid);
            int sizeP = Integer.parseInt(size);
            cart.removeItem(id, sizeP);
            ArrayList<Item> list = cart.getItems();
            session.setAttribute("cart", cart);
            session.setAttribute("sizeCart", list.size());
        } catch (Exception e) {
            System.out.println(e);
        }
        response.sendRedirect("cartdetail");
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
