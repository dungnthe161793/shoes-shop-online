/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.Client;

import dao.OrderDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.util.ArrayList;
import model.Order;

/**
 *
 * @author Admin
 */
public class OrderDetail extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet OrderDetail</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet OrderDetail at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        OrderDAO dao = new OrderDAO();
        HttpSession session = request.getSession();

        //get id user
        int u = (int) session.getAttribute("id");

        String OId = request.getParameter("Id");

        Boolean hidden = false;

        String check = request.getParameter("check");
        String search = request.getParameter("txt");
        ArrayList<Order> otlist = new ArrayList<>();
        String flag = request.getParameter("flag");

        if (flag != null && flag.equals("1")) {
            dao.UpdateStatus(Integer.parseInt(OId));
        }

        if (OId != null && OId.isEmpty()) {
            OId = null;
        }

        if (search != null && OId != null) {
            otlist = dao.searchOrderDetailByUid(u, Integer.parseInt(OId), search);
            if (otlist.isEmpty()) {
                hidden = true;
            }
        }

        if (OId != null && search == null) {
            otlist = dao.getOrderDetailByUid(u, Integer.parseInt(OId));
        }
        Order ulist = dao.getUserInfor(u, Integer.parseInt(OId));
        request.setAttribute("hiddenSearch", hidden);
        request.setAttribute("list1", otlist);
        request.setAttribute("Id", OId);
        request.setAttribute("listOrder", ulist);
        request.getRequestDispatcher("./Common/Orderdetail.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
