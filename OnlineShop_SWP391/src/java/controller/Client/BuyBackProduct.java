/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */

package controller.Client;

import dao.ProductDAO;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;
import java.util.ArrayList;
import model.Cart;
import model.Item;
import model.Size;

/**
 *
 * @author Admin
 */
@WebServlet(name="BuyBackProduct", urlPatterns={"/buybackproduct"})
public class BuyBackProduct extends HttpServlet {
   
    /** 
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code> methods.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet BuyBackProduct</title>");  
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet BuyBackProduct at " + request.getContextPath () + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    } 

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /** 
     * Handles the HTTP <code>GET</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        HttpSession session = request.getSession(true);
        Cart cart = null;
        Object ob = session.getAttribute("cart");
        if (ob != null) {
            cart = (Cart) ob;
        } else {
            cart = new Cart();
        }
        String productId = request.getParameter("productId");
        String size = request.getParameter("sizeProduct");
        String quantity = request.getParameter("quantity");
        int quan = Integer.parseInt(quantity);
        try {
            ProductDAO dao = new ProductDAO();
            model.Product p = dao.getProductDetail(productId);
            double price = p.getSalePrices();
            Size sz = dao.getOneSizeProductById(productId, size);
            if (sz == null) {
                sz = new Size(36);
            }
            Item item = new Item(p, quan, price, sz);
            cart.addItem(item);
        } catch (NumberFormatException e) {
            quan = 1;
        }

        ArrayList<Item> listCart = cart.getItems();
        session.setAttribute("cart", cart);
        session.setAttribute("sizeCart", listCart.size());
        response.sendRedirect("cartdetail");
    } 

    /** 
     * Handles the HTTP <code>POST</code> method.
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
    throws ServletException, IOException {
        processRequest(request, response);
    }

    /** 
     * Returns a short description of the servlet.
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
