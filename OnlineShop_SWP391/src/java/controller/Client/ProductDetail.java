/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package controller.Client;

import dao.ProductDAO;
import jakarta.servlet.ServletContext;

import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpSession;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import model.FeedBack;
import model.ProductImages;
import model.RatedStar;
import model.Size;

/**
 *
 * @author Admin
 */
public class ProductDetail extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ProductDetail</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ProductDetail at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        ProductDAO dao = new ProductDAO();
        HttpSession session = request.getSession();
        //get id of product from ProductList.jsp
        String pId = request.getParameter("id");
        //get location of paging feedback
        String page_raw = request.getParameter("page");
        if (page_raw == null || page_raw.isEmpty()) {
            page_raw = "1";
        }
        int page = Integer.parseInt(page_raw);
        int currentPage = page;

        session.setAttribute("pId", pId);
        //get product detail when user click to product
        model.Product product = dao.getProductDetail(pId);
        //get sub-photos of the product 
        ArrayList<ProductImages> listImg = dao.getAllImageProductbyID(pId);
        //get size of product by id
        ArrayList<Size> listSz = dao.getSizeProductById(pId);
        for (Size size : listSz) {
            if (size.getSizeName() == 36) {
                request.setAttribute("size36", size);
            }
            if (size.getSizeName() == 37) {
                request.setAttribute("size37", size);
            }
            if (size.getSizeName() == 38) {
                request.setAttribute("size38", size);
            }
            if (size.getSizeName() == 39) {
                request.setAttribute("size39", size);
            }
            if (size.getSizeName() == 40) {
                request.setAttribute("size40", size);
            }
            if (size.getSizeName() == 41) {
                request.setAttribute("size41", size);
            }
            if (size.getSizeName() == 42) {
                request.setAttribute("size42", size);
            }
            if (size.getSizeName() == 43) {
                request.setAttribute("size43", size);
            }
        }
        //get list product
        ArrayList<model.Product> listP = (ArrayList<model.Product>) session.getAttribute("listP");
        //get feedback product by id
        ArrayList<FeedBack> listFe = dao.getFeedbackProductById(pId, page);
        //get rated star and percentage of each type of product by id
        ArrayList<RatedStar> listPct = dao.getPercentRatedStar(pId);
        for (RatedStar ratedStar : listPct) {
            if (ratedStar.getRatedStar() == 1) {
                request.setAttribute("ratedStar1", ratedStar);
            }
            if (ratedStar.getRatedStar() == 2) {
                request.setAttribute("ratedStar2", ratedStar);
            }
            if (ratedStar.getRatedStar() == 3) {
                request.setAttribute("ratedStar3", ratedStar);
            }
            if (ratedStar.getRatedStar() == 4) {
                request.setAttribute("ratedStar4", ratedStar);
            }
            if (ratedStar.getRatedStar() == 5) {
                request.setAttribute("ratedStar5", ratedStar);
            }
            if (ratedStar.getRatedStar() == 0) {
                request.setAttribute("ratedStar0", ratedStar);
            }
        }

        //paging for feedback product
        int pageS = 5;
        int endPageFe = 0;
        int count = dao.getTotalFeedBackProduct(pId);
        endPageFe = count / pageS;
        if (count % pageS != 0) {
            endPageFe++;
        }

        //post on jsp
        request.setAttribute("idP", pId);
        request.setAttribute("currentPage", currentPage);
        request.setAttribute("endPageFe", endPageFe);
        request.setAttribute("listP", listP);
        request.setAttribute("listFe", listFe);
        request.setAttribute("product", product);
        request.setAttribute("ProductImg", listImg);
        request.getRequestDispatcher("./Common/ProductDetail.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    public static String formatDate(Date date, String format) {
        SimpleDateFormat dateFormat = new SimpleDateFormat(format);
        return dateFormat.format(date);
    }
}
