<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%-- 
    Document   : BlogDetail
    Created on : Jun 5, 2023, 2:15:17 PM
    Author     : quyde
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Obaju : e-commerce template</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="robots" content="all,follow">
        <!-- Favicon-->
        <link rel="shortcut icon" href="../favicon.png">
        <!-- Tweaks for older IEs--><!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
            <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->
        <link
            href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css"
            rel="stylesheet"
            />
        <!-- Google Fonts -->
        <link
            href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap"
            rel="stylesheet"
            />
        <!-- MDB -->
        <link
            href="https://cdnjs.cloudflare.com/ajax/libs/mdb-ui-kit/6.2.0/mdb.min.css"
            rel="stylesheet"
            />
        <!-- Bootstrap CSS-->
        <link rel="stylesheet" href="vendor/bootstrap/css/bootstrap.min.css">
        <!-- Font Awesome CSS-->
        <link rel="stylesheet" href="vendor/font-awesome/css/font-awesome.min.css">
        <!-- Google fonts - Roboto -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,700">
        <!-- owl carousel-->
        <link rel="stylesheet" href="vendor/owl.carousel/assets/owl.carousel.css">
        <link rel="stylesheet" href="vendor/owl.carousel/assets/owl.theme.default.css">
        <!-- theme stylesheet-->
        <link rel="stylesheet" href="css/style.default.css" id="theme-stylesheet">
        <!-- Custom stylesheet - for your changes-->
        <link rel="stylesheet" href="css/custom.css">
    </head>
    <body>
        <jsp:include page="../Common/Header.jsp"/>
        <div id="all">
            <div id="content">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <!-- breadcrumb-->
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="home">Home</a></li>
                                    <li class="breadcrumb-item"><a href="categoryblog">Quay lại danh sách blog</a></li>
                                    <!--                                    <li aria-current="page" class="breadcrumb-item active"></li>-->
                                </ol>
                            </nav>
                        </div>
                        <div class="box">
                            <h1 class="col-lg-12">Tìm kiếm tin tức:</h1>
                            <div  class="col-md-6">
                                <form role="search" class="ml-auto" action="searchblog" method="post">
                                    <div class="input-group">
                                        <input type="text" placeholder="Search" class="form-control" name="txt">
                                        <div class="input-group-append">
                                            <button type="submit" class="btn btn-primary"><i class="fa fa-search"></i></button>
                                        </div>

                                    </div>
                                </form>
                            </div>
                        </div>
                        <div id="blog-post" class="col-lg-9">
                            <div class="box">
                                <h1>${detail.title}</h1>
                                <p class="author-date">By ${detail.u.fullName}  |  ${detail.updateDate}  |  ${detail.cb.categoryBlogName} </p>
                                <p class="lead">${detail.briefInfor}</p>
                                <div id="post-content">

                                    <p><img src="img/${detail.thumbnail}" alt="Example blog post alt" class="img-fluid"></p>

                                    <p>${detail.content}<p>

                                </div>

                                <div id="comments">

                                </div>
                                <!-- /#comments-->
                                <div id="comment-form">
                                        
                                </div>
                                <!-- /#comment-form-->
                            </div>
                            <!-- /.box-->
                        </div>
                        <!-- /#blog-post-->
                        <div class="col-lg-3">
                            <!--
                            *** BLOG MENU ***
                            _________________________________________________________
                            -->
                            <div class="card sidebar-menu mb-4">

                                <div class="card-header">
                                    <h3 class="h4 panel-title">Blog</h3>
                                </div>
                                <div class="card-body">
                                    <ul class="nav flex-column nav-pills">

                                        <c:forEach var="o" items="${listCB}">
                                            <li><a href="categorycontrol?cateId=${o.categoryBlogId}" class="nav-link"${o.categoryBlogId==cateId? 'active':'' }>${o.categoryBlogName}</a></li>
                                            </c:forEach>
                                    </ul>
                                </div>
                            </div>
                            <!-- /.col-lg-3-->
                            <!-- *** BLOG MENU END ***-->
                            <!--              <div class="banner"><a href="#"><img src="img/banner.jpg" alt="sales 2014" class="img-fluid"></a></div>-->
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--
        *** FOOTER ***
        _________________________________________________________
        -->
        <jsp:include page="../Common/Footer.jsp"/>  
        <!-- *** COPYRIGHT END ***-->
        <!-- JavaScript files-->
        <script src="vendor/jquery/jquery.min.js"></script>
        <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
        <script src="vendor/jquery.cookie/jquery.cookie.js"></script>
        <script src="vendor/owl.carousel/owl.carousel.min.js"></script>
        <script src="vendor/owl.carousel2.thumbs/owl.carousel2.thumbs.js"></script>
        <script src="js/front.js"></script>
    </body>
</html>