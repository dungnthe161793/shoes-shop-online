<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%-- 
    Document   : ProductDetail
    Created on : Jun 3, 2023, 8:43:07 PM
    Author     : Admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="vi">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Obaju : e-commerce template</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="robots" content="all,follow">
        <!-- Favicon-->
        <link rel="shortcut icon" href="../favicon.png">
        <!-- Tweaks for older IEs--><!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
            <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->
        <link
            href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css"
            rel="stylesheet"
            />
        <!-- Google Fonts -->
        <link
            href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap"
            rel="stylesheet"
            />
        <!-- MDB -->
        <link
            href="https://cdnjs.cloudflare.com/ajax/libs/mdb-ui-kit/6.2.0/mdb.min.css"
            rel="stylesheet"
            />
        <!-- Bootstrap CSS-->
        <link rel="stylesheet" href="../vendor/bootstrap/css/bootstrap.min.css">
        <!-- Font Awesome CSS-->
        <link rel="stylesheet" href="../vendor/font-awesome/css/font-awesome.min.css">
        <!-- Google fonts - Roboto -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,700">
        <!-- owl carousel-->
        <link rel="stylesheet" href="../vendor/owl.carousel/assets/owl.carousel.css">
        <link rel="stylesheet" href="../vendor/owl.carousel/assets/owl.theme.default.css">
        <!-- theme stylesheet-->
        <link rel="stylesheet" href="css/style.default.css" id="theme-stylesheet">
        <!-- Custom stylesheet - for your changes-->
        <link rel="stylesheet" href="../css/custom.css">
        <style>
            .size1{
                display: flex;
                text-align: center;
                margin-top: 10px;
            }

            .img-fluid{
                height: 480px;
                max-height: 100%;
                max-width: 100%;
            }
            .customer-img{
                max-width: 100px;
                max-height: 100px;
                width: 100%;
            }

            .img-flex{
                display: flex;
            }

            .rating-list {
                display: inline-flex;
                margin-bottom: 15px;
                width: 100%;
            }
            .rating-list-left {
                height: 16px;
                line-height: 29px;
                width: 10%;
            }
            .rating-list-center {
                width: 80%;
            }
            .rating-list-right {
                line-height: 29px;
                text-align: right;
                width: 10%;
            }
            .restaurant-slider-pics {
                bottom: 0;
                font-size: 12px;
                left: 0;
                z-index: 999;
                padding: 0 10px;
            }
            .restaurant-slider-view-all {
                bottom: 15px;
                right: 15px;
                z-index: 999;
            }

            .progress {
                background: #f2f4f8 none repeat scroll 0 0;
                height: 30px;
            }
            .checked{

                color: orange;
            }
            .brief-infor p{
                text-align: left;
                color: orange;
            }

            .rating-list-right {
                text-align: center;
            }

            .rating-list-left {
                text-align: center;
            }
            .feedback-left{
                padding-left:60px;
            }
            .feedback-right{
                padding-right:60px;
            }

            .hot-product{
                padding: 0 45px;
            }
            .product{
                border: 1px black solid;
            }
            .quantity1{
                display: flex;
                text-align:center;
                justify-content:left;
            }

            .quantity1 button{
                outline: none;
                background: none;
                border: 1px solid #ececec;
                cursor: pointer;
            }

            .quantity1 button:hover{
                background-color: #ececec;
            }
            #quantity{
                width: 40px;
                text-align: center;
                border: 1px solid #ececec;
            }

            .price-product{
                display: flex;
            }

            .price-product span:first-child{
                text-decoration: line-through;
                color: #ccc;
                margin-right: 10px;
            }
            #content #productMain .price{
                margin: 10px 0;
                margin-right: 20px;
            }
            .n-sd {
                margin-left:5px;
            }

            .swatch-element {
                position: relative;
                border: 1px solid #ccc;
                padding:10px;
                background-color: #f2f4f8;
            }

            .swatch-element input[type="radio"] {
                display: none;
            }

            .swatch-element span {
                cursor: pointer;
                font-size: 12px;
            }

            .swatch-element label span {
                display: block;
                padding: 0 20px;
            }

            .swatch-element:hover {
                background-color: #0397d6;
            }

            .swatch-element input[type="radio"]:checked + label + span{
                font-weight: bold;
                font-size: 20px;
                color: #4fbfa8;
            }

            .des{
                margin-left: 370px;
            }

            .des1{
                text-align: right;
            }

            @keyframes blink {
                0% {
                    opacity: 1;
                }
                50% {
                    opacity: 0;
                }
                100% {
                    opacity: 1;
                }
            }

            .blink {
                animation: blink 1s infinite;
            }
        </style>
        <script src="../js/funtionJs.js" type="text/javascript"></script>
    </head>
    <body>
        <jsp:include page="../Common/Header.jsp"/>
        <c:set var="c" value="${sessionScope.cart}"/>
        <div id="all">
            <div id="content">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <!-- breadcrumb-->
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="home">Trang chủ</a></li>
                                    <li class="breadcrumb-item"><a href="product?page=1">Sản Phẩm</a></li>

                                    <li aria-current="page" class="breadcrumb-item active">Chi Tiết sản phẩm</li>
                                </ol>
                            </nav>
                        </div>

                        <!-- *** MENUS AND FILTERS END ***--> 

                        <div class="col-lg-12 order-1 order-lg-2">
                            <div id="productMain" class="row">
                                <div class="col-md-6">
                                    <div data-slider-id="1" class="owl-carousel shop-detail-carousel">

                                        <div id="imageContainer" class="border p-3 ">
                                            <!-- Ảnh trong khung -->
                                            <img style="width: 500px; max-width: 100%; " id="mainImage" src="img/${product.productImg.images}" class="img-fluid img-main">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <form name="pull" action="" method="post">
                                        <div class="box">
                                            <h3 style="text-align: left;">${product.productname}</h3>
                                            <input type="hidden" name="productId" id="productId" value="<%=request.getParameter("id")%>">
                                            <div class="brief-infor">
                                                <p>${product.briefInfor}</p>
                                            </div>
                                            <div class="price-product">
                                                <span class="price">${product.originalPrices}VND</span>
                                                <span class="price text-danger">${product.salePrices}VND</span>
                                            </div>
                                            <p>Kích thước:</p>
                                            <div>
                                                <p>
                                                <div class="size1">
                                                    <c:if test="${size36.status}">
                                                        <div class="n-sd swatch-element size-pro">
                                                            <input class="variant-1" type="radio" name="sizeProduct" value="${size36.sizeName}" id="radio-${size36.sizeName!=null?size36.sizeName:36}" onclick="handleRadioChange(this)">
                                                            <input type="hidden" id="hiddenInput${size36.sizeName}" value="${size36.quantity}">
                                                            <label for="radio-${size36.sizeName!=null?size36.sizeName:36}">
                                                                <span>${size36.sizeName!=null?size36.sizeName:36}</span>
                                                            </label>
                                                        </div>
                                                    </c:if>
                                                    <c:if test="${!size36.status}">
                                                        <div class="n-sd swatch-element">
                                                            <input class="variant-1" type="radio" name="sizeProduct" value="0" disabled>
                                                            <label>
                                                                <span style="pointer-events: none;"><img src="Icon/het-hang.png" width="15" height="15" alt="alt"/>${size36.sizeName!=null?size36.sizeName:36}</span>
                                                            </label>
                                                        </div>
                                                    </c:if>
                                                    <c:if test="${size37.status}">
                                                        <div class="n-sd swatch-element size-pro">
                                                            <input class="variant-1" type="radio" name="sizeProduct" value="${size37.sizeName}" id="radio-${size37.sizeName!=null?size37.sizeName:37}" onclick="handleRadioChange(this)">
                                                            <input type="hidden" id="hiddenInput${size37.sizeName}" value="${size37.quantity}">
                                                            <label for="radio-${size37.sizeName!=null?size37.sizeName:37}">
                                                                <span>${size37.sizeName!=null?size37.sizeName:37}</span>
                                                            </label>
                                                        </div>
                                                    </c:if>
                                                    <c:if test="${!size37.status}">
                                                        <div class="n-sd swatch-element">
                                                            <input class="variant-1" type="radio" name="sizeProduct" value="0" disabled>
                                                            <label>
                                                                <span style="pointer-events: none;"><img src="Icon/het-hang.png" width="15" height="15" alt="alt"/>${size37.sizeName!=null?size37.sizeName:37}</span>
                                                            </label>
                                                        </div>
                                                    </c:if>
                                                    <c:if test="${size38.status}">
                                                        <div class="n-sd swatch-element size-pro">
                                                            <input class="variant-1" type="radio" name="sizeProduct" value="${size38.sizeName}" id="radio-${size38.sizeName!=null?size38.sizeName:38}" onclick="handleRadioChange(this)">
                                                            <input type="hidden" id="hiddenInput${size38.sizeName}" value="${size38.quantity}">
                                                            <label for="radio-${size38.sizeName!=null?size38.sizeName:38}">
                                                                <span>${size38.sizeName!=null?size38.sizeName:38}</span>
                                                            </label>
                                                        </div>
                                                    </c:if>
                                                    <c:if test="${!size38.status}">
                                                        <div class="n-sd swatch-element">
                                                            <input class="variant-1" type="radio" name="sizeProduct" value="0" disabled>
                                                            <label>
                                                                <span style="pointer-events: none;"><img src="Icon/het-hang.png" width="15" height="15" alt="alt"/>${size38.sizeName!=null?size38.sizeName:38}</span>
                                                            </label>
                                                        </div>
                                                    </c:if>
                                                    <c:if test="${size39.status}">
                                                        <div class="n-sd swatch-element size-pro">
                                                            <input class="variant-1" type="radio" name="sizeProduct" value="${size39.sizeName}" id="radio-${size39.sizeName!=null?size39.sizeName:39}" onclick="handleRadioChange(this)">
                                                            <input type="hidden" id="hiddenInput${size39.sizeName}" value="${size39.quantity}">
                                                            <label for="radio-${size39.sizeName!=null?size39.sizeName:39}">
                                                                <span>${size39.sizeName!=null?size39.sizeName:39}</span>
                                                            </label>
                                                        </div>
                                                    </c:if>
                                                    <c:if test="${!size39.status}">
                                                        <div class="n-sd swatch-element">
                                                            <input class="variant-1" type="radio" name="sizeProduct" value="0" disabled>
                                                            <label>
                                                                <span style="pointer-events: none;"><img src="Icon/het-hang.png" width="15" height="15" alt="alt"/>${size39.sizeName!=null?size39.sizeName:39}</span>
                                                            </label>
                                                        </div>
                                                    </c:if>
                                                    <c:if test="${size40.status}">
                                                        <div class="n-sd swatch-element size-pro">
                                                            <input class="variant-1" type="radio" name="sizeProduct" value="${size40.sizeName}" id="radio-${size40.sizeName!=null?size40.sizeName:40}" onclick="handleRadioChange(this)">
                                                            <input type="hidden" id="hiddenInput${size40.sizeName}" value="${size40.quantity}">
                                                            <label for="radio-${size40.sizeName!=null?size40.sizeName:40}">
                                                                <span>${size40.sizeName!=null?size40.sizeName:40}</span>
                                                            </label>
                                                        </div>
                                                    </c:if>
                                                    <c:if test="${!size40.status}">
                                                        <div class="n-sd swatch-element">
                                                            <input class="variant-1" type="radio" name="sizeProduct" value="0" disabled>
                                                            <label>
                                                                <span style="pointer-events: none;"><img src="Icon/het-hang.png" width="15" height="15" alt="alt"/>${size40.sizeName!=null?size40.sizeName:40}</span>
                                                            </label>
                                                        </div>
                                                    </c:if>
                                                </div>
                                                <div class="size1">
                                                    <c:if test="${size41.status}">
                                                        <div class="n-sd swatch-element size-pro">
                                                            <input class="variant-1" type="radio" name="sizeProduct" value="${size41.sizeName}" id="radio-${size41.sizeName!=null?size41.sizeName:41}" onclick="handleRadioChange(this)">
                                                            <input type="hidden" id="hiddenInput${size41.sizeName}" value="${size41.quantity}">
                                                            <label for="radio-${size41.sizeName!=null?size41.sizeName:41}">
                                                                <span>${size41.sizeName!=null?size41.sizeName:41}</span>
                                                            </label>
                                                        </div>
                                                    </c:if>
                                                    <c:if test="${!size41.status}">
                                                        <div class="n-sd swatch-element">
                                                            <input class="variant-1" type="radio" name="sizeProduct" value="0" disabled>
                                                            <label>
                                                                <span style="pointer-events: none;"><img src="Icon/het-hang.png" width="15" height="15" alt="alt"/>${size41.sizeName!=null?size41.sizeName:41}</span>
                                                            </label>
                                                        </div>
                                                    </c:if>
                                                    <c:if test="${size42.status}">
                                                        <div class="n-sd swatch-element size-pro">
                                                            <input class="variant-1" type="radio" name="sizeProduct" value="${size42.sizeName}" id="radio-${size42.sizeName!=null?size42.sizeName:42}" onclick="handleRadioChange(this)">
                                                            <input type="hidden" id="hiddenInput${size42.sizeName}" value="${size42.quantity}">
                                                            <label for="radio-${size42.sizeName!=null?size42.sizeName:42}">
                                                                <span>${size42.sizeName!=null?size42.sizeName:42}</span>
                                                            </label>
                                                        </div>
                                                    </c:if>
                                                    <c:if test="${!size42.status}">
                                                        <div class="n-sd swatch-element">
                                                            <input class="variant-1" type="radio" name="sizeProduct" value="0" disabled>
                                                            <label>
                                                                <span style="pointer-events: none;"><img src="Icon/het-hang.png" width="15" height="15" alt="alt"/>${size42.sizeName!=null?size42.sizeName:42}</span>
                                                            </label>
                                                        </div>
                                                    </c:if>
                                                    <c:if test="${size43.status}">
                                                        <div class="n-sd swatch-element size-pro">
                                                            <input class="variant-1" type="radio" name="sizeProduct" value="${size43.sizeName}" id="radio-${size43.sizeName!=null?size43.sizeName:43}" onclick="handleRadioChange(this)">
                                                            <input type="hidden" id="hiddenInput${size43.sizeName}" value="${size43.quantity}">
                                                            <label for="radio-${size43.sizeName!=null?size43.sizeName:43}">
                                                                <span>${size43.sizeName!=null?size43.sizeName:43}</span>
                                                            </label>
                                                        </div>
                                                    </c:if>
                                                    <c:if test="${!size43.status}">
                                                        <div class="n-sd swatch-element">
                                                            <input class="variant-1" type="radio" name="sizeProduct" value="0" disabled>
                                                            <label>
                                                                <span style="pointer-events: none;"><img src="Icon/het-hang.png" width="15" height="15" alt="alt"/>${size43.sizeName!=null?size43.sizeName:43}</span>
                                                            </label>
                                                        </div>
                                                    </c:if>
                                                </div>
                                                </p>
                                            </div>
                                            <div class="quantity1">
                                                <button style="width: 30px;height: 30px;" type="button" onclick="decreaseQuantity()">-</button>
                                                <input  style="width: 60px;height: 30px;" type="number" name="quantity" id="quantity" value="1" min="1" readonly>
                                                <button style="margin-right: 30px; width: 30px; height: 30px;" type="button" onclick="increaseQuantity()">+</button>
                                                <p>Hiện có: </p>
                                                <p id="output" style="color: red;"></p>
                                            </div>
                                            <div style="display: flex;">

                                            </div>
                                            <br>
                                            <p class="text-center buttons"><button  class="btn btn-primary" type="submit" onclick="addToCart()">Thêm vào giỏ hàng<i class="fa fa-shopping-cart"></i></button></p>
                                        </div>
                                    </form>
                                    <div class="d-flex flex-column">
                                        <div class="img-flex">
                                            <!-- Ảnh nhỏ bên ngoài -->
                                            <img src="img/${product.productImg.images}" class="img-thumbnail mt-2 customer-img" onclick="changeImage('img/${product.productImg.images}')">
                                            <c:forEach var="i" items="${ProductImg}">
                                                <img src="img/${i.images}" class="img-thumbnail mt-2 customer-img" onclick="changeImage('img/${i.images}')">
                                            </c:forEach>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="details" class="box">
                                <p></p>
                                <h4>Mô Tả Sản Phẩm</h4>
                                <p>${product.getProductDetails()}</p>
                            </div>
                        </div>
                        <div class="col-lg-12">
                        </div>
                    </div>
                </div>  
                <!--feedback sản phẩm--> 
                <div class="row ">
                    <div class="col-md-6 feedback-left">
                        <div class="bg-white rounded shadow-sm p-4 mb-4 restaurant-detailed-ratings-and-reviews">
                            <h4  class="mb-1">Phản Hồi Khách Hàng</h4>
                            <c:if test="${listFe.isEmpty()}">
                                <hr>
                                <p style="color: red;">Chưa có đánh giá nào cho sản phẩm này</p>
                            </c:if>
                            <c:if test="${!listFe.isEmpty()}">
                                <c:forEach var="i" items="${listFe}">
                                    <c:if test="${i.isStatus()}">
                                        <div class="reviews-members pt-4 pb-4">
                                            <div class="media">
                                                <div class="media-body">
                                                    <div class="reviews-members-header">
                                                        <h6 class="mb-1"> 
                                                            <c:if test="${i.getUser().getAvatar() == null}">
                                                                <img alt="" src="img/backup_avatar.jpg" class="mr-3 rounded-pill" style="width: 24px; height: 24px;  margin-right:  15px;">
                                                            </c:if>
                                                            <c:if test="${i.getUser().getAvatar() != null}">
                                                                <img alt="" src="LoadImg/${i.getUser().getAvatar()}" class="mr-3 rounded-pill" style="width: 24px; height: 24px; margin-right:  15px;">
                                                            </c:if> ${i.getUser().getFullName()} (${i.getDate()})  
                                                        </h6>
                                                        <c:if test="${i.ratedStar == 5}">
                                                            <c:forEach end="${i.ratedStar}" begin="1">
                                                                <span class="fa fa-star checked"></span>
                                                            </c:forEach>
                                                        </c:if>
                                                        <c:if test="${i.ratedStar < 5}">
                                                            <c:forEach end="${i.ratedStar}" begin="1">
                                                                <span class="fa fa-star checked"></span>
                                                            </c:forEach>
                                                            <c:forEach end="${5 - i.ratedStar}" begin="1">
                                                                <span class="fa fa-star"></span>
                                                            </c:forEach>
                                                        </c:if> 
                                                        <p style="margin-top: 10px;"><strong>${i.getFullName()}: </strong>${i.getFeedback()}</p>
                                                    </div>
                                                    <div class="reviews-members-body">
                                                        <c:if test="${i.getImage() != null}">
                                                            <img src="LoadImg/${i.getImage()}" alt="Không thể tải ảnh"  width="150" height="100"/>   
                                                        </c:if>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </c:if>
                                </c:forEach>
                                <div class="pages">
                                    <nav aria-label="Page navigation example" class="d-flex justify-content-center">
                                        <ul class="pagination">
                                            <li class="page-item <c:if test='${currentPage == 1}'>disabled</c:if>">
                                                <a href="productdetail?id=${idP}&page=${currentPage - 1}" aria-label="Previous" class="page-link">
                                                    <span aria-hidden="true">«</span><span class="sr-only">Previous</span>
                                                </a>
                                            </li>
                                            <c:forEach var="i" begin="1" end="${endPageFe}">
                                                <li class="page-item">
                                                    <a href="productdetail?id=${idP}&page=${i}" class="page-link ${i == page_raw? 'active':''}">${i}</a>
                                                </li>
                                            </c:forEach>
                                            <li class="page-item <c:if test='${currentPage == endPageFe}'>disabled</c:if>">
                                                <a href="productdetail?id=${idP}&page=${currentPage + 1}" aria-label="Next" class="page-link">
                                                    <span aria-hidden="true">»</span><span class="sr-only">Next</span>
                                                </a>
                                            </li>
                                        </ul>
                                    </nav>
                                </div>
                            </c:if>
                        </div>
                    </div>
                    <div class="col-md-6 feedback-right">
                        <div class="tab-pane fade active show" id="pills-reviews" role="tabpanel" aria-labelledby="pills-reviews-tab">
                            <div class="bg-white rounded shadow-sm p-4 mb-4 clearfix graph-star-rating">
                                <h4 class="mb-0 mb-4">Thống kê mức độ đánh giá sản phẩm</h4>
                                <div class="graph-star-rating-body">
                                    <div class="rating-list">
                                        <div class="rating-list-left text-black">
                                            ${ratedStar5.ratedStar==null?5:ratedStar5.ratedStar} <span class="fa fa-star checked"></span>
                                        </div>
                                        <div class="rating-list-center">
                                            <div class="progress">
                                                <div style="width: ${ratedStar5.votePercentage==null?0:ratedStar5.votePercentage}%;" aria-valuemax="5" aria-valuemin="0" aria-valuenow="5" role="progressbar" class="progress-bar bg-primary">
                                                    <span class="sr-only">${ratedStar5.votePercentage==null?0:ratedStar5.votePercentage}</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="rating-list-right text-black">${ratedStar5.votePercentage==null?0:ratedStar5.votePercentage}%</div>
                                    </div>
                                    <div class="rating-list">
                                        <div class="rating-list-left text-black">
                                            ${ratedStar4.ratedStar==null?4:ratedStar4.ratedStar} <span class="fa fa-star checked"></span>
                                        </div>
                                        <div class="rating-list-center">
                                            <div class="progress">
                                                <div style="width: ${ratedStar4.votePercentage==null?0:ratedStar4.votePercentage}%;" aria-valuemax="5" aria-valuemin="0" aria-valuenow="5" role="progressbar" class="progress-bar bg-primary">
                                                    <span class="sr-only">${ratedStar4.votePercentage==null?0:ratedStar4.votePercentage}</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="rating-list-right text-black">${ratedStar4.votePercentage==null?0:ratedStar4.votePercentage}%</div>
                                    </div>
                                    <div class="rating-list">
                                        <div class="rating-list-left text-black">
                                            ${ratedStar3.ratedStar==null?3:ratedStar3.ratedStar} <span class="fa fa-star checked"></span>
                                        </div>
                                        <div class="rating-list-center">
                                            <div class="progress">
                                                <div style="width: ${ratedStar3.votePercentage==null?0:ratedStar3.votePercentage}%;" aria-valuemax="5" aria-valuemin="0" aria-valuenow="5" role="progressbar" class="progress-bar bg-primary">
                                                    <span class="sr-only">${ratedStar3.votePercentage==null?0:ratedStar3.votePercentage}</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="rating-list-right text-black">${ratedStar3.votePercentage==null?0:ratedStar3.votePercentage}%</div>
                                    </div>
                                    <div class="rating-list">
                                        <div class="rating-list-left text-black">
                                            ${ratedStar2.ratedStar==null?2:ratedStar2.ratedStar} <span class="fa fa-star checked"></span>
                                        </div>
                                        <div class="rating-list-center">
                                            <div class="progress">
                                                <div style="width:${ratedStar2.votePercentage==null?0:ratedStar2.votePercentage}%;" aria-valuemax="5" aria-valuemin="0" aria-valuenow="5" role="progressbar" class="progress-bar bg-primary">
                                                    <span class="sr-only">${ratedStar2.votePercentage==null?0:ratedStar2.votePercentage}</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="rating-list-right text-black">${ratedStar2.votePercentage==null?0:ratedStar2.votePercentage}%</div>
                                    </div>
                                    <div class="rating-list">
                                        <div class="rating-list-left text-black">
                                            ${ratedStar1.ratedStar==null?1:ratedStar1.ratedStar} <span class="fa fa-star checked"></span>
                                        </div>
                                        <div class="rating-list-center">
                                            <div class="progress">
                                                <div style="width: ${ratedStar1.votePercentage==null?0:ratedStar1.votePercentage}%;" aria-valuemax="5" aria-valuemin="0" aria-valuenow="5" role="progressbar" class="progress-bar bg-primary">
                                                    <span class="sr-only">${ratedStar1.votePercentage==null?0:ratedStar1.votePercentage}</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="rating-list-right text-black">${ratedStar1.votePercentage==null?0:ratedStar1.votePercentage}%</div>
                                    </div>
                                    <div class="rating-list">
                                        <div class="rating-list-left text-black">
                                            ${ratedStar0.ratedStar==null?0:ratedStar0.ratedStar}<span class="fa fa-star checked"></span>
                                        </div>
                                        <div class="rating-list-center">
                                            <div class="progress">
                                                <div style="width: ${ratedStar0.votePercentage==null?0:ratedStar0.votePercentage}%" aria-valuemax="5" aria-valuemin="0" aria-valuenow="5" role="progressbar" class="progress-bar bg-primary">
                                                    <span class="sr-only">${ratedStar0.votePercentage==null?0:ratedStar0.votePercentage}</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="rating-list-right text-black">${ratedStar0.votePercentage==null?0:ratedStar0.votePercentage}%</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row hot-product">
                    <c:forEach var="i" items="${sessionScope.listP}">
                        <c:if test="${i.status}">
                            <div class="col-md-2 col-sm-6">
                                <div class="product same-height">
                                    <div class="flip-container">
                                        <div class="flipper">
                                            <div class="front"><a href="productdetail?id=${i.productid}"><img style="height: 150px; width: 200px;" src="img/${i.productImg.images}" alt="${i.productImg.images}" class="img-fluid ed-img"></a></div>
                                            <div class="back"><a href="productdetail?id=${i.productid}"><img style="height: 150px; width: 200px;" src="img/${i.productImg.images}" alt="" class="img-fluid ed-img"></a></div>
                                        </div>
                                    </div><a href="productdetail?id=${i.productid}" class="invisible"><img style="height: 150px; width: 200px;" src="img/${i.productImg.images}" alt="" class="img-fluid ed-img"></a>
                                    <div class="text">
                                        <h3><a href="productdetail?id=${i.productid}">${i.productname}</a></h3>
                                        <div class="blink" style="font-style: italic; text-align: center;" >
                                            Sale:
                                            <span class="price text-danger ">${i.salePrices}VND</span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </c:if>
                    </c:forEach>  
                </div>
            </div>
        </div>
    </div>

    <!--
    *** FOOTER ***
    _________________________________________________________
    -->
    <jsp:include page="../Common/Footer.jsp"/>        <!-- *** COPYRIGHT END ***-->
    <!-- JavaScript files-->

    <script type="text/javascript">
        var hiddenInputValue;

        function changeImage(imageSrc) {
            // Thay đổi ảnh chính trong khung
            document.getElementById('mainImage').src = imageSrc;
        }


        function handleRadioChange(radio) {
            if (radio.checked) {
                var hiddenInput = document.getElementById("hiddenInput" + radio.value);
                var output = document.getElementById("output");
                output.innerText = hiddenInput.value;
                hiddenInputValue = hiddenInput.value;
            }
        }

        function decreaseQuantity() {
            var quantityInput = document.getElementById("quantity");
            var currentQuantity = parseInt(quantityInput.value);

            if (currentQuantity > 1) {
                quantityInput.value = currentQuantity - 1;
            }
        }

        function increaseQuantity() {
            var quantityInput = document.getElementById("quantity");
            var currentQuantity = parseInt(quantityInput.value);
            if (currentQuantity < hiddenInputValue) {
                quantityInput.value = currentQuantity + 1;
            }
        }

        function addToCart() {
            document.pull.action = "cartdetail";
            document.pull.submit();
        }
        function changeMoney() {
            //format price VietNamDong
            const priceElements = document.querySelectorAll('.price');
            const formatter = new Intl.NumberFormat('vi-VN', {
                style: 'currency',
                currency: 'VND'
            });
            priceElements.forEach(element => {
                const price = parseFloat(element.textContent);
                element.textContent = formatter.format(price);
            });
        }
        changeMoney();


    </script>
    <script src="../vendor/jquery/jquery.min.js"></script>
    <script src="../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="../vendor/jquery.cookie/jquery.cookie.js"></script>
    <script src="../vendor/owl.carousel/owl.carousel.min.js"></script>
    <script src="../vendor/owl.carousel2.thumbs/owl.carousel2.thumbs.js"></script>
    <script src="../js/front.js"></script> 
</body>
</html>