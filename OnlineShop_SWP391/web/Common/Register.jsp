

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>


        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.3.0/css/all.min.css" integrity="sha512-SzlrxWUlpfuzQ+pcUCosxcglQRNAq/DZjVsC0lE40xsADsfeQoEypE+enwcOiGjk/bSuGGKHEyjSoQ1zVisanQ==" crossorigin="anonymous" referrerpolicy="no-referrer" /><link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">

        <style>

            container{
                height: 100vh
            }
            .card{
                width: 100%;
                padding: 30px
            }
            .form{
                padding: 20px
            }
            .form-control{
                height: 50px;
                background-color: #eee
            }
            .form-control:focus{
                color: #495057;
                background-color: #fff;
                border-color: #f50057;
                outline: 0;
                box-shadow: none;
                background-color: #eee
            }
            .inputbox{
                margin-bottom: 15px
            }
            .register{
                width: 200px;
                height: 51px;
                background-color: #f50057;
                border-color: #f50057
            }
            .register:hover{
                width: 200px;
                height: 51px;
                background-color: #f50057;
                border-color: #f50057
            }
            .login{
                color: #f50057;
                text-decoration: none
            }
            .login:hover{
                color: #f50057;
                text-decoration: none
            }
            .form-check-input:checked{
                background-color: #f50057;
                border-color: #f50057
            }
        </style>




        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Tạo tài khoản mới</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="robots" content="all,follow">
        <!-- Bootstrap CSS-->
        <link rel="stylesheet" href="vendor/bootstrap/css/bootstrap.min.css">
        <!-- Font Awesome CSS-->
        <link rel="stylesheet" href="vendor/font-awesome/css/font-awesome.min.css">
        <!-- Google fonts - Roboto -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,700">
        <!-- owl carousel-->
        <link rel="stylesheet" href="vendor/owl.carousel/assets/owl.carousel.css">
        <link rel="stylesheet" href="vendor/owl.carousel/assets/owl.theme.default.css">
        <!-- theme stylesheet-->
        <link rel="stylesheet" href="css/style.default.css" id="theme-stylesheet">
        <!-- Custom stylesheet - for your changes-->
        <link rel="stylesheet" href="css/custom.css">
        <!-- Favicon-->
        <link rel="shortcut icon" href="favicon.png">
        <!-- Tweaks for older IEs--><!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
            <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->
    </head>
    <body>
        <jsp:include page="../Common/Header.jsp"/>

        <div id="all">
            <div id="content">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <!-- breadcrumb-->
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="home">Trang chủ</a></li>
                                    <li aria-current="page" class="breadcrumb-item active">Tạo tài khoản</li>
                                </ol>
                            </nav>
                        </div>

                        <form method="post" action="register">
                            <div class="container d-flex justify-content-center align-items-center">
                                <div class="card">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form">
                                                <h2>Đăng Ký Tài Khoản</h2>
                                                <div class="inputbox mt-3">
                                                    <span>Họ Và Tên</span>
                                                    <input id="name" type="text" placeholder="Name" name="name" class="form-control">
                                                </div>
                                                <div class="inputbox mt-3">
                                                    <span>Email</span>
                                                    <input id=" email" type="email" placeholder="Email" name="email" class="form-control">
                                                </div>
                                                <div class="inputbox mt-3">
                                                    <span>Mật Khẩu</span>
                                                    <input id="password" type="password" placeholder="password" name="password" class="form-control">
                                                </div>
                                                <div class="inputbox mt-3">
                                                    <span>Số Điện Thoại</span>
                                                    <input id="mobile" type="text" placeholder="+1 455 445 4532" name="mobile" class="form-control">
                                                </div>
                                                <div class="inputbox mt-3">
                                                    <span>Địa Chỉ</span>
                                                    <input id="address" type="text" placeholder="address" name="address" class="form-control">
                                                </div>
                                                <div class="inputbox mt-3">
                                                    <span>Giới tính</span>
                                                    <input type="radio" name="gender" value="1"> Nam
                                                    <input type="radio" name="gender" value="0"> Nữ
                                                </div>
                                                <p style="color: red">${error}</p>
                                                <div class="d-flex justify-content-between align-items-center">
                                                    <div class="text-right">
                                                        <button type="submit" class="btn btn-success register btn-block"><i class="fa fa-user-md"></i>Đăng ký tài khoản</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="text-center mt-5">
                                                <img src="./img/Loginpic.png" width="400">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>


                    </div>
                </div>
            </div>
        </div>
        <jsp:include page="../Common/Footer.jsp"/> 
        <!-- JavaScript files-->
        <script src="vendor/jquery/jquery.min.js"></script>
        <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
        <script src="vendor/jquery.cookie/jquery.cookie.js"></script>
        <script src="vendor/owl.carousel/owl.carousel.min.js"></script>
        <script src="vendor/owl.carousel2.thumbs/owl.carousel2.thumbs.js"></script>
        <script src="js/front.js"></script>
    </body>
</html>