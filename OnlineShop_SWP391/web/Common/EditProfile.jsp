<%-- 
    Document   : signup
    Created on : Apr 21, 2023, 1:50:36 PM
    Author     : asus
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>


        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.3.0/css/all.min.css" integrity="sha512-SzlrxWUlpfuzQ+pcUCosxcglQRNAq/DZjVsC0lE40xsADsfeQoEypE+enwcOiGjk/bSuGGKHEyjSoQ1zVisanQ==" crossorigin="anonymous" referrerpolicy="no-referrer" /><link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Obaju : e-commerce template</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="robots" content="all,follow">
        <!-- Bootstrap CSS-->
        <link rel="stylesheet" href="vendor/bootstrap/css/bootstrap.min.css">
        <!-- Font Awesome CSS-->
        <link rel="stylesheet" href="vendor/font-awesome/css/font-awesome.min.css">
        <!-- Google fonts - Roboto -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,700">
        <!-- owl carousel-->
        <link rel="stylesheet" href="vendor/owl.carousel/assets/owl.carousel.css">
        <link rel="stylesheet" href="vendor/owl.carousel/assets/owl.theme.default.css">
        <!-- theme stylesheet-->
        <link rel="stylesheet" href="css/style.default.css" id="theme-stylesheet">
        <!-- Custom stylesheet - for your changes-->
        <link rel="stylesheet" href="css/custom.css">
        <!-- Favicon-->
        <link rel="shortcut icon" href="favicon.png">
        <!-- Tweaks for older IEs--><!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
            <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->
    </head>
    <body>
        <jsp:include page="../Common/Header.jsp"/>

        <div id="all">
            <div id="content">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-12">
                            <!-- breadcrumb-->
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb">
                                    <li class="breadcrumb-item"><a href="home">Trang chủ</a></li>
                                    <li aria-current="page" class="breadcrumb-item active">Thông tin khách hàng</li>
                                </ol>
                            </nav>
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                <div class="card sidebar-menu">
                                    <div class="card-header">
                                        <h3 class="h4 card-title">Thông tin khách hàng</h3>
                                    </div>
                                    <div class="card-body">
                                        <ul class="nav nav-pills flex-column"><a style="color: #20c997" class="nav-link"><i class="fa fa-user"></i>Thông tin cá nhân</a><a style="color: #20c997" href="logout" class="nav-link"><i class="fa fa-sign-out"></i> Đăng xuất</a></ul>
                                    </div>
                                </div>
                            </div>
                            <form method="post" action="editprofile" class="col-md-9" enctype="multipart/form-data" >
                                <input type="hidden" class="form-control-sm" name="id" value="${profile.userId}">
                                <div class="container rounded bg-white mt-5 mb-5">
                                    <div class="row">
                                        <div class="col-md-5 border-left">
                                            <div class="d-flex flex-column align-items-center text-center p-3 py-5">
                                                <img class="rounded-circle mt-5" id="imagePreview" src="LoadImg/${profile.avatar}"width="200" height="250" >
                                                <br>
                                                <input style="margin-left: 80px;" name="avatar" type="file" accept="image/*"  onchange="loadFile(event)">
                                                <br>
                                                <span class="text-black-50">Email: <input type="email" class="form-control-sm" name="email" value="${profile.email}" readonly></span>
                                            </div>
                                        </div>
                                        <div class="col-md-7 border-right">
                                            <div class="p-3 py-5">
                                                <div class="d-flex justify-content-between align-items-center mb-3">
                                                    <h2 class="text-right">Thông tin cá nhân</h2>
                                                </div>
                                                <div class="row col-md-9">
                                                    <label class="labels">Họ và tên</label>
                                                    <input type="text" class="form-control" name="name" placeholder="First Name" value="${profile.fullName}" >
                                                </div>
                                                <br><!-- comment -->
                                                <div class="row col-md-9">
                                                    <label class="labels">Địa chỉ</label>
                                                    <input type="text" name="address"  class="form-control" value="${profile.address}">
                                                </div><br><!-- comment -->
                                                <div class="row col-md-9">
                                                    <label class="labels">Số điện thoại</label>
                                                    <input type="text" class="form-control" name="phone"  value="${profile.mobile}">
                                                </div><br><!-- comment -->
                                                <div class="row mt-2">
                                                    <div class="col-md-6">
                                                        <label class="labels">Giới tính</label>
                                                        <input type="radio" name="gender" value="1" ${profile.gender ? "checked" : ""}>Male
                                                        <input type="radio" name="gender" value="0" ${!profile.gender ? "checked" : ""}>Female
                                                    </div>
                                                </div>
                                                <div class="mt-5 text-center">
                                                    <input onclick="confirmSubmit()" type="submit" value="Submit" class="btn btn-primary profile-button" >
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script>
            function  confirmSubmit() {
                let text = "Cập nhật thành công";
                if (confirm(text) == true) {
                    document.getElementById("form1").submit();
                } else {
                }
            }


            var loadFile = function (event) {
                var output = document.getElementById('imagePreview');
                output.src = URL.createObjectURL(event.target.files[0]);
                output.onload = function () {
                    URL.revokeObjectURL(output.src) // free memory
                }
            };
        </script>
        <jsp:include page="../Common/Footer.jsp"/> 
        <!-- JavaScript files-->
        <script src="vendor/jquery/jquery.min.js"></script>
        <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
        <script src="vendor/jquery.cookie/jquery.cookie.js"></script>
        <script src="vendor/owl.carousel/owl.carousel.min.js"></script>
        <script src="vendor/owl.carousel2.thumbs/owl.carousel2.thumbs.js"></script>
        <script src="js/front.js"></script>
    </body>
</html>