<%-- 
    Document   : login
    Created on : May 25, 2023, 8:44:00 AM
    Author     : asus
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Change Password</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="robots" content="all,follow">
        <!-- Favicon-->
        <link rel="shortcut icon" href="../favicon.png">
        <!-- Tweaks for older IEs--><!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
            <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script><![endif]-->
        <link
            href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.0.0/css/all.min.css"
            rel="stylesheet"
            />
        <!-- Google Fonts -->
        <link
            href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap"
            rel="stylesheet"
            />
        <!-- MDB -->
        <link
            href="https://cdnjs.cloudflare.com/ajax/libs/mdb-ui-kit/6.2.0/mdb.min.css"
            rel="stylesheet"
            />
        <!-- Bootstrap CSS-->
        <link rel="stylesheet" href="vendor/bootstrap/css/bootstrap.min.css">
        <!-- Font Awesome CSS-->
        <link rel="stylesheet" href="vendor/font-awesome/css/font-awesome.min.css">
        <!-- Google fonts - Roboto -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,700">
        <!-- owl carousel-->
        <link rel="stylesheet" href="vendor/owl.carousel/assets/owl.carousel.css">
        <link rel="stylesheet" href="vendor/owl.carousel/assets/owl.theme.default.css">
        <!-- theme stylesheet-->
        <link rel="stylesheet" href="css/style.default.css" id="theme-stylesheet">
        <!-- Custom stylesheet - for your changes-->
        <link rel="stylesheet" href="css/custom.css">
        
    </head>
    <body>
        <jsp:include page="../Common/Header.jsp"/>
        <div>
            <div id="all">
                <div id="content">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-12">
                            </div>
                            <div class="container h-100">
                                <div class="row d-flex justify-content-center align-items-center h-100">
                                    <div class="col-lg-12 col-xl-11">
                                        <div class="card text-black" style="border-radius: 25px;">
                                            <div class="card-body p-md-5">
                                                <div class="row justify-content-center">
                                                    <div class="col-md-10 col-lg-6 col-xl-5 order-2 order-lg-1">

                                                        <p class="text-center h2 fw-bold mb-5 mx-1 mx-md-4 mt-4">Đổi mật khẩu</p>
                                                        <% if(request.getAttribute("error")!=null){%>
                                                        <p  h6" style="margin-left: 60px; color: red; margin-top: -30px" ><% out.print((String)request.getAttribute("error"));  }
                                                            %>  </p>
                                                            <% if(request.getAttribute("success")!=null){%>
                                                        <p  h6" style="margin-left: 60px; color: blue; margin-top: -30px" ><% out.print((String)request.getAttribute("success"));  }
                                                            %>  </p>
                                                        <form class="mx-1 mx-md-4" action="changepassword" method="post">

                                                            <div class="d-flex flex-row align-items-center mb-4">
                                                                <i class="fas fa-user fa-lg me-3 fa-fw"></i>
                                                                <div class="form-outline flex-fill mb-0">
                                                                    <input type="text" id="form3Example1c" class="form-control" readonly value="<%= (String)request.getAttribute("email") %>" />
                                                                    <label class="form-label" for="form3Example1c">Email</label>
                                                                </div>
                                                            </div>

                                                            <div class="d-flex flex-row align-items-center mb-4">
                                                                <i class="fas fa-lock fa-lg me-3 fa-fw"></i>
                                                                <div class="form-outline flex-fill mb-0">
                                                                    <input type="password" name="oldpassword" class="form-control" />
                                                                    <label class="form-label" for="form3Example4c">Mật khẩu hiện tại</label>
                                                                </div>
                                                            </div>

                                                            <div class="d-flex flex-row align-items-center mb-4">
                                                                <i class="fas fa-lock fa-lg me-3 fa-fw"></i>
                                                                <div class="form-outline flex-fill mb-0">
                                                                    <input type="password" name="newpassword" class="form-control" />
                                                                    <label class="form-label" for="form3Example4c">Mật khẩu mới</label>
                                                                </div>
                                                            </div>

                                                            <div class="d-flex flex-row align-items-center mb-4">
                                                                <i class="fas fa-key fa-lg me-3 fa-fw"></i>
                                                                <div class="form-outline flex-fill mb-0">
                                                                    <input type="password" name="renewpassword" class="form-control" />
                                                                    <label class="form-label" for="form3Example4cd">Nhập lại mật khẩu mới</label>
                                                                </div>
                                                            </div>
                                                            <div class="d-flex flex-row align-items-center mb-4">
                                                                <i class="fas fa-question fa-lg me-3 fa-fw"></i>
                                                                <div class="form-outline flex-fill mb-0">
                                                                    <a href="forgotpassword" class="text-body"><b>Quên mật khẩu?</b></a>
                                                                </div>
                                                            </div>


                                                            <div class="d-flex justify-content-center mx-4 mb-3 mb-lg-4">
                                                                <button type="submit" class="btn btn-primary btn-lg"> Lưu thay đổi </button>
                                                            </div>


                                                        </form>

                                                    </div>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>



                        </div>
                    </div>
                </div>
            </div>
        </div>                                                               
        <jsp:include page="../Common/Footer.jsp"/>        <!-- *** COPYRIGHT END ***-->
        <!-- JavaScript files-->
        <script src="vendor/jquery/jquery.min.js"></script>
        <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
        <script src="vendor/jquery.cookie/jquery.cookie.js"></script>
        <script src="vendor/owl.carousel/owl.carousel.min.js"></script>
        <script src="vendor/owl.carousel2.thumbs/owl.carousel2.thumbs.js"></script>
        <script src="js/front.js"></script> 
        <script
            type="text/javascript"
            src="https://cdnjs.cloudflare.com/ajax/libs/mdb-ui-kit/6.2.0/mdb.min.js"
        ></script>
    </body>
</html>
