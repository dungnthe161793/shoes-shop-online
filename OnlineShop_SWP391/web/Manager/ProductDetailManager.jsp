<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">

    <head>
        <title>Chỉnh sửa sản phẩm | Quản trị Admin</title>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Main CSS-->
        <link rel="stylesheet" type="text/css" href="css/main.css">
        <!-- Font-icon css-->
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/boxicons@latest/css/boxicons.min.css">
        <!-- or -->
        <link rel="stylesheet" href="https://unpkg.com/boxicons@latest/css/boxicons.min.css">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.2/sweetalert.min.js"></script>
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css">
        <link rel="stylesheet" type="text/css"
              href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <script type="text/javascript" src="/ckeditor/ckeditor.js"></script>
        <script src="http://code.jquery.com/jquery.min.js" type="text/javascript"></script>
    </head>

    <body class="app sidebar-mini rtl">
        <style>
            .Choicefile {
                display: block;
                background: #14142B;
                border: 1px solid #fff;
                color: #fff;
                width: 150px;
                text-align: center;
                text-decoration: none;
                cursor: pointer;
                padding: 5px 0px;
                border-radius: 5px;
                font-weight: 500;
                align-items: center;
                justify-content: center;
            }

            .Choicefile:hover {
                text-decoration: none;
                color: white;
            }

            #uploadfile,
            .removeimg {
                display: none;
            }

            #thumbbox {
                position: relative;
                width: 100%;
                margin-bottom: 20px;
            }

            .removeimg {
                height: 25px;
                position: absolute;
                background-repeat: no-repeat;
                top: 5px;
                left: 5px;
                background-size: 25px;
                width: 25px;
                /* border: 3px solid red; */
                border-radius: 50%;

            }

            .removeimg::before {
                -webkit-box-sizing: border-box;
                box-sizing: border-box;
                content: '';
                border: 1px solid red;
                background: red;
                text-align: center;
                display: block;
                margin-top: 11px;
                transform: rotate(45deg);
            }

            .removeimg::after {
                /* color: #FFF; */
                /* background-color: #DC403B; */
                content: '';
                background: red;
                border: 1px solid red;
                text-align: center;
                display: block;
                transform: rotate(-45deg);
                margin-top: -2px;
            }
            .swatch-element label{
                margin-right: 20px;
            }
            .swatch-element:hover {
                color: #0397d6;
            }

            .swatch-element input[type="radio"]:checked + label {
                font-weight: bold;
                font-size: 20px;
                color: #4fbfa8;
            }

            .input-number{
                border: 1px solid white;
                width: 80px;
            }
        </style>
        <jsp:include page="../Manager/Dashbroad.jsp"/> 
        <main class="app-content">
            <div class="app-title">
                <ul class="app-breadcrumb breadcrumb">
                    <li class="breadcrumb-item"><a href="productlistmanager">Danh sách sản phẩm \ </a></li>
                    <li style="color: #138496">  Chi tiết sản phẩm</li>
                </ul>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="tile">
                        <h3 class="tile-title">Chi tiết sản phẩm</h3>
                        <div class="tile-body">
                            <div class="row">
                                <div class="form-group col-md-3">
                                    <label class="control-label">Mã sản phẩm </label>
                                    <input class="form-control" type="text" value="${p.productid}" readonly>
                                </div>
                                <div class="form-group col-md-9">
                                    <label class="control-label">Tên sản phẩm</label>
                                    <input class="form-control" type="text" value="${p.productname}" readonly>
                                </div>
                            </div>
                            <div class="row">
                                <div class="form-group col-md-3">
                                    <label for="exampleSelect1" class="control-label">Danh mục</label>
                                    <input class="form-control" type="text" value="${p.category.categoryName}" readonly>
                                </div>
                                <div class="form-group col-md-3">
                                    <label for="exampleSelect1" class="control-label">Tình trạng</label>
                                    <div style="display: block;">
                                        <c:if test="${p.status}">
                                            <input class="form-control" type="text" value="Còn hàng" readonly>
                                        </c:if>
                                        <c:if test="${!p.status}">
                                            <input class="form-control" type="text" value="Hết hàng" readonly>
                                        </c:if>
                                    </div>
                                </div>
                                <div class="form-group col-md-3">
                                    <label class="control-label">Giá bán</label>
                                    <input class="form-control" type="text" value="${p.salePrices}" readonly>
                                </div>
                                <div class="form-group col-md-3">
                                    <label class="control-label">Giá vốn</label>
                                    <input class="form-control" type="text" value="${p.originalPrices}" readonly>
                                </div>
                            </div>
                            <div class="form-group col-md-12">
                                <label class="control-label">Tiêu đề</label>
                                <textarea class="form-control" rows="1" readonly>${p.getBriefInfor()}</textarea>
                            </div>
                            <div class="form-group col-md-12">
                                <label class="control-label">Mô tả sản phẩm</label>
                                <textarea class="form-control" readonly>${p.getProductDetails()}</textarea>
                            </div>
                        </div>
                        <div class="row">
                            <div class="form-group col-md-4">
                                <label class="control-label">Ảnh chính cho sản phẩm</label>
                                <img src="img/${p.productImg.images}" alt="Ảnh chính sản phẩm" width="200" height="200"/>
                            </div>
                            <div class="col-md-4">
                                <table>
                                    <thead>
                                        <tr>
                                            <th>Size</th>
                                            <th>Số lượng</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <c:forEach var="i" items="${size}">
                                            <tr>
                                                <td style="width: 100px;">${i.sizeName}</td>
                                                <td><input class="input-number" type="text" value="${i.quantity}" readonly></td>
                                            </tr>
                                        </c:forEach>
                                    </tbody>
                                </table>
                            </div>
                            <div class="col-md-4">
                                <table border="1">
                                    <thead>
                                        <tr>
                                            <th>Ảnh phụ</th>
                                        </tr>
                                    </thead>
                                    <c:if test="${listSubImg != null}">
                                        <tbody>
                                            <c:forEach var="i" items="${listSubImg}">
                                                <tr>
                                                    <td><img src="img/${i.images}" alt="Ảnh phụ" width="100" height="100"/></td>
                                                </tr>
                                            </c:forEach>
                                        </tbody>
                                    </c:if>
                                    <c:if test="${listSubImg == null}">
                                        <tbody>
                                            <c:forEach var="i" items="${listSubImg}">

                                                <tr>
                                                    <td>Không có ảnh phụ</td>
                                                </tr>
                                            </c:forEach>
                                        </tbody>
                                    </c:if>
                                </table>
                            </div>
                        </div>
                        <div class="row" style="margin-top: 30px;">
                            <div class="form-group col-md-12" style="text-align: right;">
                                <a style="width: 200px; height: 50px;" class="btn btn-cancel" href="editproductmanager?id=${p.productid}" onclick="confirmCancel(event);">Chỉnh sửa sản phẩm</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
    <script src="js/jquery-3.2.1.min.js"></script>
    <script src="js/popper.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/main.js"></script>
    <script src="js/plugins/pace.min.js"></script>
</body>

</html>