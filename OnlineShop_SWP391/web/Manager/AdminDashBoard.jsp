<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%-- 
    Document   : AdminDashBoard
    Created on : Jul 7, 2023, 2:40:19 PM
    Author     : quyde
--%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">

    <head>
        <title>Danh sách nhân viên | Quản trị Admin</title>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Main CSS-->
        <link rel="stylesheet" type="text/css" href="css/main.css">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/boxicons@latest/css/boxicons.min.css">
        <!-- or -->
        <link rel="stylesheet" href="https://unpkg.com/boxicons@latest/css/boxicons.min.css">
        <!-- Font-icon css-->
        <link rel="stylesheet" type="text/css"
              href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.2/sweetalert.min.js"></script>
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css">

    </head>
    
    <body onload="time()" class="app sidebar-mini rtl">
        <!-- Sidebar menu-->
        <div class="app-sidebar__overlay" data-toggle="sidebar"></div>
        <jsp:include page="../Manager/Dashbroad.jsp"/>    
        <main class="app-content">
            <div class="row">
                <div class="col-md-12">
                    <div class="app-title">
                        <ul class="app-breadcrumb breadcrumb">
                            <li class="breadcrumb-item"><a href="#"><b>Báo cáo doanh thu</b></a></li>
                        </ul>
                        <div id="clock"></div>
                    </div>
                </div>
            </div>
            <div class="row-container"> 
                <div class="row">

                    <div class="col-md-6 col-lg-3">
                        <div class="widget-small warning coloured-icon"><i class='icon bx bx-run fa-3x'></i>
                            <div class="info">
                                <h6 style="color: red">Tổng đơn hàng đang vận chuyển</h6>
                                <p><b>${totalordertrans}</b></p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-lg-3">
                        <div class="widget-small primary coloured-icon"><i class='icon bx bx-check-shield fa-3x'></i>
                            <div class="info">
                                <h6 style="color: red">Tổng đơn hàng giao thành công</h6>
                                <p><b>${totalordersuccess}</b></p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-lg-3">
                        <div class="widget-small danger coloured-icon"><i class='icon fa-3x bx bx-x-circle'></i>
                            <div class="info">
                                <h6 style="color: red">Tổng đơn hàng bị hủy</h6>
                                <p><b>${totalordercancel}</b></p>
                            </div>
                        </div>
                    </div>                <div class="col-md-6 col-lg-3">
                        <div class="widget-small info coloured-icon"><i class='icon fa-3x bx bxs-user-badge' ></i>
                            <div class="info">
                                <h6 style="color: red">Tổng số khách hàng</h6>
                                <p><b>${totalcustomer}</b></p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 col-lg-3">
                        <div class="widget-small primary coloured-icon"><i class='icon fa-3x bx bxs-chart' ></i>
                            <div class="info">
                                <h6 style="color: red">Tổng thu nhập</h6>
                                <p><b><fmt:formatNumber value="${totalrevenue}" pattern="#,### đ" /></b></p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-lg-3">
                        <div class="widget-small info coloured-icon"><i class='icon fa-3x bx bxs-user-plus' ></i>
                            <div class="info">
                                <h6 style="color: red">Khách hàng mới đăng ký</h6>
                                <p><b>${uRe.fullName}</b></p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-lg-3">
                        <div class="widget-small primary coloured-icon"><i class='icon fa-3x bx bxs-user-plus' ></i>
                            <div class="info">
                                <h6 style="color: red">Khách hàng mới mua sản phẩm</h6>
                                <p><b>${uBo.fullName}</b></p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-lg-3">
                        <div class="widget-small primary coloured-icon">
                            <i class="icon fa-3x bx bxs-star" style="color:
                               <c:choose>
                                   <c:when test="${avgrate < 3}">
                                       red
                                   </c:when>
                                   <c:when test="${avgrate >= 3 && avgrate <= 4}">
                                       yellow
                                   </c:when>
                                   <c:otherwise>
                                       green
                                   </c:otherwise>
                               </c:choose>
                               "></i>
                            <div class="info">
                                <h6 style="color: red">Avg rated star</h6>
                                <p><b>${avgrate}</b></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>              

            <div class="row">
                <div class="col-md-12">
                    <div class="tile">
                        <div>
                            <h3 class="tile-title">SẢN PHẨM BÁN CHẠY</h3>
                        </div>
                        <div class="tile-body">
                            <table class="table table-hover table-bordered" id="sampleTable">
                                <thead>
                                    <tr>
                                        <th>Mã sản phẩm</th>
                                        <th>Tên sản phẩm</th>
                                        <th>Giá gốc</th>
                                        <th>Giá sale</th>
                                        <th>Tên danh mục</th>
                                        <th>Số lượng đã bán</th>

                                    </tr>
                                </thead>
                                <tbody>
                                    <c:forEach var="o" items="${listp}">
                                        <tr>
                                            <td>${o.productid}</td>
                                            <td>${o.productname}</td>
                                            <td><fmt:formatNumber value="${o.originalPrices}" pattern="#,### đ" /></td>
                                            <td><fmt:formatNumber value="${o.salePrices}" pattern="#,### đ" />
                                            <td>${o.category.categoryName}</td>
                                            <td>${o.totalQuantity}</td>
                                        </tr>
                                    </c:forEach>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="tile">
                        <div>
                            <h3 class="tile-title">Tổng doanh thu theo từng danh mục sản phẩm</h3>
                        </div>
                        <div class="tile-body">
                            <table class="table table-hover table-bordered" id="sampleTable">
                                <thead>
                                    <tr>
                                        <th>Danh mục sản phẩm</th>
                                        <th>Doanh thu</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <c:forEach var="o" items="${totalrevenuebycategory}">
                                        <tr>
                                            <td>${o.ca.categoryName}</td>
                                            <td><fmt:formatNumber value="${o.totalCost}" pattern="#,### đ" /></td>

                                        </tr>
                                    </c:forEach>
                                    <tr>
                                        <td>Tổng cộng:</td>
                                        <td><p><b><fmt:formatNumber value="${totalrevenue}" pattern="#,### đ" /></b></p></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="tile">
                        <div>
                            <h3 class="tile-title">Số lượt phản hồi của từng danh mục sản phẩm</h3>
                        </div>
                        <div class="tile-body">
                            <table class="table table-hover table-bordered" id="sampleTable">
                                <thead>
                                    <tr>
                                        <th>Danh mục sản phẩm</th>
                                        <th>Số luợt phản hồi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <c:forEach var="o" items="${totalfbbycategory}">
                                        <tr>
                                            <td>${o.c.categoryName}</td>
                                            <td>${o.feedbackId}</td>
                                        </tr>
                                    </c:forEach>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="tile">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="tile">
                                    <form action="admindashboard" method="post">
                                        <input type="date" name="d1" value="${d1}" class="form-control form-control-sm">
                                        <input type="date" name="d2" value="${d2}" class="form-control form-control-sm">
                                        <button type="submit" class="btn btn-success register btn-block"><i class="fa fa-user-md"></i> Lọc</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div>
                            <h3 class="tile-title">Xu hướng đơn hàng</h3>
                        </div>
                        <div class="tile-body">
                            <table class="table table-hover table-bordered" id="sampleTable">
                                <thead>
                                    <tr>
                                        <th>Xu hướng của số lượng đơn hàng đặt thành công</th>
                                        <th>Xu hướng của số lượng tất cả đơn hàng</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>${totalsuc7}</td>
                                        <td>${totalall7}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-12">
                <div class="tile">
                    <h3 class="tile-title">Thống kê tình trạng đơn</h3>
                    <div class="col-md-6 mb-4" >
                        <canvas id="myChart" width="600" height="700"></canvas>
                    </div>
                </div>
            </div>

            <div class="text-right" style="font-size: 12px">

            </div>
        </main>
        <!-- Essential javascripts for application to work-->
        <script src="js/jquery-3.2.1.min.js"></script>
        <script src="js/popper.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/main.js"></script>
        <!-- The javascript plugin to display page loading on top-->
        <script src="js/plugins/pace.min.js"></script>
        <!-- Page specific javascripts-->
        <script type="text/javascript" src="js/plugins/chart.js"></script>

        <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
        <script type="text/javascript"></script>

        <script>
        const ctx = document.getElementById('myChart');
        const dataValues = [
            <c:forEach var="value" items="${dataValues}">
                ${value},
            </c:forEach>
        ];
        new Chart(ctx, {
            type: 'bar',
            data: {
                labels: ['Order Transport', 'Order Success', 'Order Cancel', ],
                datasets: [{
                        label: 'Number of order',
                        data: dataValues,
                        borderWidth: 1
                    }]
            },
            options: {
                scales: {
                    y: {
                        beginAtZero: true
                    }
                }
            }
        });

        </script>  
        <!-- Google analytics script-->
        <script type="text/javascript">
            if (document.location.hostname == 'pratikborsadiya.in') {
                (function (i, s, o, g, r, a, m) {
                    i['GoogleAnalyticsObject'] = r;
                    i[r] = i[r] || function () {
                        (i[r].q = i[r].q || []).push(arguments);
                    }, i[r].l = 1 * new Date();
                    a = s.createElement(o),
                            m = s.getElementsByTagName(o)[0];
                    a.async = 1;
                    a.src = g;
                    m.parentNode.insertBefore(a, m);
                })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');
                ga('create', 'UA-72504830-1', 'auto');
                ga('send', 'pageview');
            }
        </script>
    </body>

</html>