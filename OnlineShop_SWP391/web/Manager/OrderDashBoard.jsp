<%-- 
    Document   : DashBoard
    Created on : Jul 5, 2023, 9:43:45 PM
    Author     : ADMIN
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html lang="en">
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <head>
        <title>Danh sách nhân viên | Quản trị Admin</title>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Main CSS-->
        <link rel="stylesheet" type="text/css" href="css/main.css">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/boxicons@latest/css/boxicons.min.css">
        <!-- or -->
        <link rel="stylesheet" href="https://unpkg.com/boxicons@latest/css/boxicons.min.css">
        <!-- Font-icon css-->
        <link rel="stylesheet" type="text/css"
              href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.2/sweetalert.min.js"></script>
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css">

    </head>
    <style>
        .form-container {


            .form-container

            .form-container {
                display: flex;
                flex-direction: column;
                align-items: center;
                justify-content: center;
                height: 100vh;
            }

            .form-container form {
                display: flex;
                flex-direction: column;
                align-items: center;
                justify-content: center;
                margin-bottom: 1rem;
            }

            .form-container select,
            .form-container button {
                margin: 0.5rem;
            }
        </style>
        <body onload="time()" class="app sidebar-mini rtl">
            <jsp:include page="../Manager/Dashbroad.jsp"/> 
            <main class="app-content">
                <div class="row">
                    <div class="col-md-12">
                        <div class="app-title">
                            <ul class="app-breadcrumb breadcrumb">
                                <li class="breadcrumb-item"><a href="#"><b>Bảng điều khiển</b></a></li>
                            </ul>
                            <div id="clock"></div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <!--Left-->
                    <div class="col-md-12 col-lg-6">
                        <div class="row">
                            <!-- col-6 -->
                            <div class="col-md-6">
                                <div class="widget-small primary coloured-icon"><i class='icon bx bxs-user-account fa-3x'></i>
                                    <div class="info">
                                        <h6 style="color: red">Tổng người dùng</h6>
                                        <p><b>${a}</b></p>
                                        <p class="info-tong">Tổng số khách hàng .</p>
                                    </div>
                                </div>
                            </div>
                            <!-- col-6 -->

                            <div class="col-md-6">
                                <div class="widget-small warning coloured-icon"><i class='icon bx bxs-shopping-bags fa-3x'></i>
                                    <div class="info">
                                        <h6 style="color: red">Tổng đơn hàng</h6>
                                            
                                        <p><b>${y}</b></p>
                                        <p class="info-tong">Tổng số đơn hàng.</p>
                                    </div>
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="tile">
                                    <h3 class="tile-title">Sản phẩm bán chạy nhất</h3>
                                    <div>
                                        <table class="table table-hover">
                                            <thead>
                                                <tr>
                                                    <th>ID</th>
                                                    <th>Tên Sản Phẩm</th>
                                                    <th>Tổng_số lượng</th>

                                                </tr>
                                            </thead>
                                            <tbody>
                                                <c:forEach items="${list}" var="c">   
                                                    <tr>
                                                        <td>${c.productid}</td>
                                                        <td>${c.productname}</td> 
                                                        <td>${c.totalQuantity}</td>
                                                    </tr>                    
                                                </c:forEach>
                                            </tbody>
                                        </table>
                                    </div>

                                </div>
                            </div>
                            <!-- / col-12 -->
                        </div>
                    </div>

                    <div class="col-md-12 col-lg-6">
                        <div class="row">

                            <div class="col-md-12">
                                <div class="tile">
                                    <h3 class="tile-title">Thống kê doanh thu</h3>
                                    <form action="orderdashboard" method="post">
                                        <select name="op">
                                            <c:forEach var="i" items="${listSale}">
                                                <option ${option1 == i.userId?"selected":""} value="${i.userId}">${i.fullName}</option>
                                            </c:forEach>
                                        </select>
                                        <button type="submit" class="filter-button">Lọc</button>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="tile">


                                                    <input type="date" name="d1" value="${d1}" class="form-control form-control-sm">


                                                    <input type="date" name="d2" value="${d2}" class="form-control form-control-sm">





                                                </div>
                                            </div>
                                        </div>

                                    </form>
                                    <div class="col-md-12" >
                                        <canvas id="myChart" width="120" height="80"></canvas>
                                    </div>

                                </div>
                            </div>


                            <!--                        <div class="col-md-12">
                                                        <div class="tile">
                                                            <h3 class="tile-title">Thống kê 7 ngày doanh thu</h3>
                                                            <div class="embed-responsive embed-responsive-16by9">
                                                                <canvas class="embed-responsive-item" id="barChartDemo"></canvas>
                                                            </div>
                                                        </div>
                                                    </div>-->
                        </div>

                    </div>
                    <!--END right-->
                </div>


            </main>
            <script src="js/jquery-3.2.1.min.js"></script>
            <!--===============================================================================================-->
            <script src="js/popper.min.js"></script>
            <script src="https://unpkg.com/boxicons@latest/dist/boxicons.js"></script>
            <!--===============================================================================================-->
            <script src="js/bootstrap.min.js"></script>
            <!--===============================================================================================-->
            <script src="js/main.js"></script>
            <!--===============================================================================================-->
            <script src="js/plugins/pace.min.js"></script>
            <!--===============================================================================================-->
            <script type="text/javascript" src="js/plugins/chart.js"></script>
            <!--===============================================================================================-->
            <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>


            <script>
            const ctx = document.getElementById('myChart');
            const dataValues = [
                <c:forEach var="value" items="${dataValues}">
                    ${value},
                </c:forEach>
            ];
            new Chart(ctx, {
                type: 'bar',
                data: {
                    labels: ['Chờ xác nhận', 'Vận chuyển', 'Đang giao', 'Hoàn Thành', 'Đã hủy ', 'Trả hàng/Hoàn tiền'],
                    datasets: [{
                            label: '# of Votes',
                            data: dataValues,
                            borderWidth: 1
                        }]
                },
                options: {
                    scales: {
                        y: {
                            beginAtZero: true
                        }
                    }
                }
            });
            </script>

            <script type="text/javascript">
                //Thời Gian
                function time() {
                    var today = new Date();
                    var weekday = new Array(7);
                    weekday[0] = "Chủ Nhật";
                    weekday[1] = "Thứ Hai";
                    weekday[2] = "Thứ Ba";
                    weekday[3] = "Thứ Tư";
                    weekday[4] = "Thứ Năm";
                    weekday[5] = "Thứ Sáu";
                    weekday[6] = "Thứ Bảy";
                    var day = weekday[today.getDay()];
                    var dd = today.getDate();
                    var mm = today.getMonth() + 1;
                    var yyyy = today.getFullYear();
                    var h = today.getHours();
                    var m = today.getMinutes();
                    var s = today.getSeconds();
                    m = checkTime(m);
                    s = checkTime(s);
                    nowTime = h + " giờ " + m + " phút " + s + " giây";
                    if (dd < 10) {
                        dd = '0' + dd
                    }
                    if (mm < 10) {
                        mm = '0' + mm
                    }
                    today = day + ', ' + dd + '/' + mm + '/' + yyyy;
                    tmp = '<span class="date"> ' + today + ' - ' + nowTime +
                            '</span>';
                    document.getElementById("clock").innerHTML = tmp;
                    clocktime = setTimeout("time()", "1000", "Javascript");

                    function checkTime(i) {
                        if (i < 10) {
                            i = "0" + i;
                        }
                        return i;
                    }

                }
                function submitForm() {
                    document.getElementById("myForm").submit();
                }
            </script>

        </body>

    </html>
