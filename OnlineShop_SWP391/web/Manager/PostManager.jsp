<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%-- 
    Document   : PostManager
    Created on : Jun 29, 2023, 4:14:06 PM
    Author     : quyde
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">

    <head>
        <title>Danh sách quản lý Post | Quản trị Admin</title>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Main CSS-->
        <link rel="stylesheet" type="text/css" href="css/main.css">
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/boxicons@latest/css/boxicons.min.css">
        <!-- or -->
        <link rel="stylesheet" href="https://unpkg.com/boxicons@latest/css/boxicons.min.css">

        <!-- Font-icon css-->
        <link rel="stylesheet" type="text/css"
              href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.2/sweetalert.min.js"></script>
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css">


    </head>
    <style>
        img {
            width: 100%;
        }
        table {
            table-layout: auto;
            width: 100%;
        }

        td {
            width: 1%;
            white-space: nowrap;


        }
        .category-column
         {
        max-width: 150px; /* Điều chỉnh giá trị tùy theo nhu cầu */
        overflow: hidden;
        text-overflow: ellipsis;
        white-space: nowrap;
        }
        td img {
            width: 200px;
            height: 150px;
        }

        tr.hidden {
            display: none;
        }
        tr.post-row.hidden td:not(:last-child) {
            visibility: hidden;
        }
        .title {
            display: inline-block;
            max-width: 200px; /* Điều chỉnh giá trị tùy theo nhu cầu */

            overflow: hidden;
            text-overflow: ellipsis;
        }

    </style>

    <body onload="time()" class="app sidebar-mini rtl">
        <jsp:include page="../Manager/Dashbroad.jsp"/>        
        <main class="app-content">
            <div class="app-title">
                <ul class="app-breadcrumb breadcrumb side">
                    <li class="breadcrumb-item active"><a href="#"><b>Quản lý bài đăng</b></a></li>
                </ul>
                <div id="clock"></div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="tile">
                        <div class="tile-body">
                            <div class="row element-button">
                                <div class="col-sm-2">
                                    <a class="btn btn-add btn-sm" href="addpost" title="Thêm"><i class="fas fa-plus"></i>
                                        Tạo mới
                                    </a>
                                </div>

                            </div>
                            
                             <div class="row element-button">
                                <div class="col-sm-2">
                                    <div style="display: flex;">
                                        <p style="width: 120px; height: 10px; font-size: 15px; margin-right: 10px; padding: 0 10px;">Lọc sản phẩm:</p>
                                        <form id="myForm" action="postmanager" method="post">
                                            <select name="mainSelect" style="margin-right: 10px; height: 30px; width: 150px; border-radius: 5px;" onchange="submitForm()">
                                                <option ${sessionScope.option == null?"selected":""}  value="">Lựa chọn</option>
                                                <option ${sessionScope.option == "0"?"selected":""}   value="0">Danh mục bài đăng</option>
                                                <option ${sessionScope.option == "1"?"selected":""}   value="1">Trạng thái bài đăng</option>
                                                <option ${sessionScope.option == "2"?"selected":""}   value="2">Tác giả</option>
                                            </select>
                                        </form>
                                        <form id="myForm1" action="filterpostlistmanager" method="post">
                                            <c:if test="${sessionScope.option == 0}">
                                                <select style="height: 30px; border-radius: 5px;" name="filterPost" onchange="submitForm1()">
                                                    <option ${optionFilter == 0?"selected":""} value="0">Tất cả</option>
                                                    <c:forEach var="o" items="${listCB}">
                                                        <option ${optionFilter == o.categoryBlogId?"selected":""} value="${o.categoryBlogId}">${o.categoryBlogName}</option>
                                                    </c:forEach>
                                                </select>
                                            </c:if>

                                            <c:if test="${sessionScope.option == 1}">
                                                <select style="height: 30px; border-radius: 5px;" name="filterPost" onchange="submitForm1()">
                                                    <option ${optionFilter == 0?"selected":""} value="0">Tất cả</option>
                                                    <option ${optionFilter == -1000?"selected":""} value="-1000">Active</option>
                                                    <option ${optionFilter == -2000?"selected":""} value="-2000">InActive</option>
                                                </select>
                                            </c:if>
                                            <c:if test="${sessionScope.option == 2}">
                                                <select style="height: 30px; border-radius: 5px;" name="filterPost" onchange="submitForm1()">
                                                    <option ${optionFilter == 0?"selected":""} value="0">Tất cả</option>
                                                    <c:forEach var="o" items="${listAu}">
                                                        <option ${optionFilter == o.userId?"selected":""} value="${-o.userId}">${o.fullName}</option>
                                                    </c:forEach>
                                                </select>
                                            </c:if>
                                        </form>
                                    </div>
                                </div>
                            </div>                        

                            <table class="table table-hover table-bordered" id="sampleTable">
                                <thead>
                                    <tr>

                                        <th>ID</th>
                                        <th>Thumbnail</th>
                                        <th>Title</th>
                                        <th>Category</th>
                                        <th>Author</th>
                                        <th>Status</th>
                                        <th>Chức năng</th>
                                    </tr>
                                </thead>
                                <tbody>

                                    <c:forEach var="o" items="${listb}">
                                        <tr class="post-row">

                                            <td>${o.blogId}</td>
                                            <td><img src="img/${o.thumbnail}" class="column"></td>
                                            <td> <a href="postdetail?pid=${o.blogId}" class="title">${o.title}</a></td>
                                            <td class="category-column">${o.cb.categoryBlogName}</td> 
                                            <td class="author-column">${o.u.fullName}</td>
                                            <td>
                                                <span class="badge ${o.status == 'true' ? 'bg-success' : 'bg-danger'}">
                                                    ${o.status == 'true' ? 'Active' : 'Inactive'}
                                                </span>
                                            </td>
                                            <td>
                                               <button class="btn btn-primary btn-sm trash" type="button" title="Xóa"><a href="#" onclick="showMess(${o.blogId})" class="btn-link"><i class="fas fa-trash-alt"></i></a></button>
                                                <button class="btn btn-primary btn-sm edit" type="button" title="Sửa"><a href="loadpost?pid=${o.blogId}"  class="btn-link"><i class="fa fa-edit"></i></a></button>
                                                <button class="btn btn-primary btn-sm hide-slider" type="button" title="Hide" onclick="hideSlider(this)"><i class="fa fa-eye-slash"></i></button>
                                                <button class="btn btn-primary btn-sm show-slider" type="button" title="Show" onclick="showSlider(this)" style="display: none;"><i class="fa fa-eye"></i></button>
                                            </td>
                                        </tr>
                                    </c:forEach>

                                </tbody>
                            </table>

                        </div>
                    </div>
                </div>
            </div>
        </main>
        <!-- Essential javascripts for application to work-->
        <script src="js/jquery-3.2.1.min.js"></script>
        <script src="js/popper.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>   
        <script src="src/jquery.table2excel.js"></script>
        <script src="js/main.js"></script>
        <!-- The javascript plugin to display page loading on top-->
        <script src="js/plugins/pace.min.js"></script>
        <!-- Page specific javascripts-->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.js"></script>
        <!-- Data table plugin-->
        <script type="text/javascript" src="js/plugins/jquery.dataTables.min.js"></script>
        <script type="text/javascript" src="js/plugins/dataTables.bootstrap.min.js"></script>
        <script type="text/javascript">$('#sampleTable').DataTable();</script>
        <script>

            function showMess(bid) {
                var option = confirm("Are you sure to delete");
                if (option === true) {
                    window.location.href = 'deletepost?bid=' + bid;
                }
            }




            oTable = $('#sampleTable').dataTable();
            $('#all').click(function (e) {
                $('#sampleTable tbody :checkbox').prop('checked', $(this).is(':checked'));
                e.stopImmediatePropagation();
            });

            //EXCEL
            $(document).ready(function () {
                $('#').DataTable({

                    dom: 'Bfrtip',
                    "buttons": [
                        'excel'
                    ]
                });
            });


            //Thời Gian
            function time() {
                var today = new Date();
                var weekday = new Array(7);
                weekday[0] = "Chủ Nhật";
                weekday[1] = "Thứ Hai";
                weekday[2] = "Thứ Ba";
                weekday[3] = "Thứ Tư";
                weekday[4] = "Thứ Năm";
                weekday[5] = "Thứ Sáu";
                weekday[6] = "Thứ Bảy";
                var day = weekday[today.getDay()];
                var dd = today.getDate();
                var mm = today.getMonth() + 1;
                var yyyy = today.getFullYear();
                var h = today.getHours();
                var m = today.getMinutes();
                var s = today.getSeconds();
                m = checkTime(m);
                s = checkTime(s);
                nowTime = h + " giờ " + m + " phút " + s + " giây";
                if (dd < 10) {
                    dd = '0' + dd
                }
                if (mm < 10) {
                    mm = '0' + mm
                }
                today = day + ', ' + dd + '/' + mm + '/' + yyyy;
                tmp = '<span class="date"> ' + today + ' - ' + nowTime +
                        '</span>';
                document.getElementById("clock").innerHTML = tmp;
                clocktime = setTimeout("time()", "1000", "Javascript");

                function checkTime(i) {
                    if (i < 10) {
                        i = "0" + i;
                    }
                    return i;
                }
            }
            //In dữ liệu
            var myApp = new function () {
                this.printTable = function () {
                    var tab = document.getElementById('sampleTable');
                    var win = window.open('', '', 'height=700,width=700');
                    win.document.write(tab.outerHTML);
                    win.document.close();
                    win.print();
                }
            }
            //     //Sao chép dữ liệu
            //     var copyTextareaBtn = document.querySelector('.js-textareacopybtn');

            // copyTextareaBtn.addEventListener('click', function(event) {
            //   var copyTextarea = document.querySelector('.js-copytextarea');
            //   copyTextarea.focus();
            //   copyTextarea.select();

            //   try {
            //     var successful = document.execCommand('copy');
            //     var msg = successful ? 'successful' : 'unsuccessful';
            //     console.log('Copying text command was ' + msg);
            //   } catch (err) {
            //     console.log('Oops, unable to copy');
            //   }
            // });


            //Modal
//            $("#show-emp").on("click", function () {
//                $("#ModalUP").modal({backdrop: false, keyboard: false});
//                // Lấy tất cả các nút "Hide product"
//                const hideButtons = document.querySelectorAll('.btn btn-primary btn-sm hide');
//
//
//
//            });

            function hideSlider(button) {
                var row = button.parentNode.parentNode;
                var showButton = row.querySelector('.show-slider');

                // Ẩn các cột dữ liệu trong dòng
                var columns = row.querySelectorAll('td:not(:last-child)');
                for (var i = 0; i < columns.length; i++) {
                    columns[i].style.visibility = 'hidden';
                }

                // Hiển thị nút "Show" và ẩn nút "Hide"
                showButton.style.display = '';
                button.style.display = 'none';
            }

            function showSlider(button) {
                var row = button.parentNode.parentNode;
                var hideButton = row.querySelector('.hide-slider');

                // Hiển thị lại các cột dữ liệu trong dòng
                var columns = row.querySelectorAll('td:not(:last-child)');
                for (var i = 0; i < columns.length; i++) {
                    columns[i].style.visibility = 'visible';
                }

                // Hiển thị nút "Hide" và ẩn nút "Show"
                hideButton.style.display = '';
                button.style.display = 'none';
            }

           function submitForm() {
                document.getElementById("myForm").submit();
            }

            function submitForm1() {
                document.getElementById("myForm1").submit();
            }
        </script>
    </body>
