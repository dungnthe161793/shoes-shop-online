<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%-- 
    Document   : AddPost
    Created on : Jul 1, 2023, 4:52:48 PM
    Author     : quyde
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">

    <head>
        <title>Danh sách đơn hàng | Quản trị Admin</title>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- Main CSS-->
        <link href="css/main.css" rel="stylesheet" type="text/css"/>
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/boxicons@latest/css/boxicons.min.css">
        <!-- or -->
        <link rel="stylesheet" href="https://unpkg.com/boxicons@latest/css/boxicons.min.css">

        <!-- Font-icon css-->
        <link rel="stylesheet" type="text/css"
              href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.2/sweetalert.min.js"></script>
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery-confirm/3.3.2/jquery-confirm.min.css">

    </head>

    <body onload="time()" class="app sidebar-mini rtl">
        <jsp:include page="../Manager/Dashbroad.jsp"/>        
        <main class="app-content">
            <div class="app-title">
                <ul class="app-breadcrumb breadcrumb">
                    <li class="breadcrumb-item">Thêm mới bài đăng</li>
                    <li class="breadcrumb-item"><a href="postmanager">Quay lại danh sách bài đăng</a></li>
                </ul>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="tile">
                        <h3 class="tile-title">Tạo mới bảng bài đăng</h3>
                        <div class="tile-body">
                            <div>
                                <form action="addpost" method="post"  enctype="multipart/form-data"  class="row">
                                    <div class="form-group col-md-3">
                                        <label class="control-label">Title</label>
                                        <input name="title" class="form-control" type="text" required>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label class="control-label">Content</label>
                                        <input name="content" class="form-control" type="text" >
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label class="control-label">Thumbnail</label>
                                        <input id="thumbnail" name="thumbnail" class="form-control" type="file" accept="image/*" required>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label class="control-label">Brief Infor</label>
                                        <input name="briefinfor" class="form-control" type="text" >
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label for="exampleSelect1" class="control-label">Blog Category</label>
                                        <select name="categoryblog" class="form-control" id="exampleSelect1">
                                            <c:forEach items="${listCB}" var="o" >
                                                <option value="${o.categoryBlogId}">${o.categoryBlogName}</option>
                                            </c:forEach>
                                        </select>
                                    </div>    
                                    <div class="form-group col-md-3">
                                        <label for="exampleSelect1" class="control-label">Trạng thái</label>
                                        <select name="status" class="form-control" id="exampleSelect1">
                                            <option value="1">Active</option>
                                            <option value="0">Inactive</option>
                                        </select>
                                    </div>  
                                     <button style="padding: 0 30px;" class="btn btn-save" type="submit">Thêm</button>
                                    
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>

        <!--
        MODAL
        -->
        <div class="modal fade" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle"
             data-backdrop="static" data-keyboard="false">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">

                    <div class="modal-body">
                        <div class="row">
                            <div class="form-group  col-md-12">
                                <span class="thong-tin-thanh-toan">
                                    <h5>Tạo trạng thái mới</h5>
                                </span>
                            </div>
                            <div class="form-group col-md-12">
                                <label class="control-label">Nhập tên trạng thái mới</label>
                                <input class="form-control" type="text" required>
                            </div>
                        </div>
                        <BR>
                        <button class="btn btn-save" type="button">Lưu lại</button>
                        <a class="btn btn-cancel" data-dismiss="modal" href="#">Hủy bỏ</a>
                        <BR>
                    </div>
                    <div class="modal-footer">
                    </div>
                </div>
            </div>
        </div>
        <!--
        MODAL
        -->

        <!-- Essential javascripts for application to work-->
    
        <script src="js/jquery-3.2.1.min.js"></script>
        <script src="js/popper.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/main.js"></script>
        <!-- The javascript plugin to display page loading on top-->
        <script src="js/plugins/pace.min.js"></script>

    </body>

</html>

